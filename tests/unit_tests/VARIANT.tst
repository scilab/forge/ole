ref = "Allan";
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_BSTR" then pause, end
if ole_variantToScilab(r) <> ref then pause, end

ref = 1.2;
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_R8" then pause, end
if ole_variantToScilab(r) <> ref then pause, end

ref = %f;
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_BOOL" then pause, end
if ole_variantToScilab(r) <> ref then pause, end

ref = [];
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_EMPTY" then pause, end
if ole_variantToScilab(r) <> ref then pause, end

ref = int(3);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_R8" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end

ref = int8(3);
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_I1" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = int16(3);
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_I2" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = int32(3);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_INT" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end

ref = uint8(3);
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_UI1" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = uint16(3);
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_UI2" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = uint32(3);
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_UINT" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = ["Allan"; "CORNET"];
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
if ole_variantToScilab(r) <> ref then pause, end

ref = int8(ones(3,2));
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = int16(ones(3,2));
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = int32(ones(3,2));
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = uint8(ones(3,2));
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = uint16(ones(3,2));
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end

ref = uint32(ones(3,2));
tref = inttype(ref);
r = ole_scilabToVariant(ref);
if ole_getVariantType(r) <> "VT_SAFEARRAY" then pause, end
res = ole_variantToScilab(r);
if res <> ref then pause, end
if inttype(res) <> tref then pause, end
