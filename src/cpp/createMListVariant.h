/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#ifndef __CREATEMLISTVARIANT_H__
#define __CREATEMLISTVARIANT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "api_scilab.h"

SciErr createMListVariant(void* _pvCtx,
                         int _iVar,
                         int _iRows, int _iCols,
                         const wchar_t **_pwstStrings,
                         const double* _pdblReal);

#ifdef __cplusplus
};
#endif

#endif
/*--------------------------------------------------------------------------*/
