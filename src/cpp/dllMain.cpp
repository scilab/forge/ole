/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2010 */
/*--------------------------------------------------------------------------*/
#include <Windows.h>
#include <Ole2.h>
#include "dynlib_automation.h"
/*--------------------------------------------------------------------------*/
IMPORT_EXPORT_AUTOMATION_DLL BOOL WINAPI DllMain (HANDLE /*hDll*/, DWORD dwReason, LPVOID /*lpReserved*/)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		// Code to run when the DLL is loaded
		CoInitialize(NULL);
		break;

	case DLL_PROCESS_DETACH:
		// Code to run when the DLL is freed
		 CoUninitialize();
		break;

	case DLL_THREAD_ATTACH:
		// Code to run when a thread is created during the DLL's lifetime
		break;

	case DLL_THREAD_DETACH:
		// Code to run when a thread ends normally.
		break;
	}
	return TRUE;
}
/*--------------------------------------------------------------------------*/
