/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include "modeManager.hxx"
/*--------------------------------------------------------------------------*/
static bool bConversionModeAuto = true;
/*--------------------------------------------------------------------------*/
bool getVariantModeConversion(void)
{
    return bConversionModeAuto;
}
/*--------------------------------------------------------------------------*/
void setVariantModeConversion(bool newVariantModeConversion)
{
    bConversionModeAuto = newVariantModeConversion;
}
/*--------------------------------------------------------------------------*/
