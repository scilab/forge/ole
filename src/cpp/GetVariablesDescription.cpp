/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <windows.h>
#include <string>
#include <sstream>
#include <Ole2.h>
#include <OAIdl.h>
#include <comdef.h>
#include <OleAuto.h>
#include "GetVariablesDescription.hxx"
/*--------------------------------------------------------------------------*/
std::wstring stringifyCustomType(HREFTYPE refType, ITypeInfo* pti) 
{
    ITypeInfo* pCustTypeInfo;
    HRESULT hr(pti->GetRefTypeInfo(refType, &pCustTypeInfo));
    if(hr) return L"UnknownCustomType";

    BSTR bstrType;

    hr = pCustTypeInfo->GetDocumentation(-1, &bstrType, 0, 0, 0);
    if(hr) return L"UnknownCustomType";
    return _wcsdup(bstrType);
}
/*--------------------------------------------------------------------------*/
std::wstring stringifyTypeDesc(TYPEDESC* typeDesc, ITypeInfo* pTypeInfo) 
{
    std::wostringstream oss;
    if(typeDesc->vt == VT_PTR) 
    {
        oss << stringifyTypeDesc(typeDesc->lptdesc, pTypeInfo) << L"*";
        return oss.str();
    }
    if(typeDesc->vt == VT_SAFEARRAY) 
    {
        oss << L"SAFEARRAY("
            << stringifyTypeDesc(typeDesc->lptdesc, pTypeInfo) << L")";
        return oss.str();
    }

    if(typeDesc->vt == VT_CARRAY) 
    {
        oss<< stringifyTypeDesc(&typeDesc->lpadesc->tdescElem, pTypeInfo);
        for(int dim(0); typeDesc->lpadesc->cDims; ++dim) 
            oss<< L"["<< typeDesc->lpadesc->rgbounds[dim].lLbound<< L"..."
            << (typeDesc->lpadesc->rgbounds[dim].cElements + 
            typeDesc->lpadesc->rgbounds[dim].lLbound - 1) << L"]";
        return oss.str();
    }
    if(typeDesc->vt == VT_USERDEFINED) 
    {
        oss<< stringifyCustomType(typeDesc->hreftype, pTypeInfo);
        return oss.str();
    }

    switch(typeDesc->vt) 
    {
    // VARIANT/VARIANTARG compatible types
    case VT_I2: return L"short";
    case VT_I4: return L"long";
    case VT_R4: return L"float";
    case VT_R8: return L"double";
    case VT_CY: return L"CY";
    case VT_DATE: return L"DATE";
    case VT_BSTR: return L"BSTR";
    case VT_DISPATCH: return L"IDispatch*";
    case VT_ERROR: return L"SCODE";
    case VT_BOOL: return L"VARIANT_BOOL";
    case VT_VARIANT: return L"VARIANT";
    case VT_UNKNOWN: return L"IUnknown*";
    case VT_UI1: return L"BYTE";
    case VT_DECIMAL: return L"DECIMAL";
    case VT_I1: return L"char";
    case VT_UI2: return L"USHORT";
    case VT_UI4: return L"ULONG";
    case VT_I8: return L"__int64";
    case VT_UI8: return L"unsigned __int64";
    case VT_INT: return L"int";
    case VT_UINT: return L"UINT";
    case VT_HRESULT: return L"HRESULT";
    case VT_VOID: return L"void";
    case VT_LPSTR: return L"char*";
    case VT_LPWSTR: return L"wchar_t*";
    }
    return L"BIG ERROR!";
}
/*--------------------------------------------------------------------------*/
std::wstring stringifyVarDesc(VARDESC* varDesc, ITypeInfo* pti) 
{
    //CComPtr<ITypeInfo> pTypeInfo(pti);
    std::wostringstream oss;
    if(varDesc->varkind == VAR_CONST) oss<< L"const ";
    oss << stringifyTypeDesc(&varDesc->elemdescVar.tdesc, pti);

    //CComBSTR bstrName;
    BSTR bstrName;

    HRESULT hr(pti->GetDocumentation(varDesc->memid, &bstrName, 0, 0, 0));

    if(hr) 
    {
            oss<< L"UnknownName";
            return oss.str();
    }

    oss<< L' '<< _wcsdup(bstrName);

    if(varDesc->varkind != VAR_CONST) return oss.str();

    oss<< L" = ";

    VARIANT variant;
    //CComVariant variant;

    hr = VariantChangeType(&variant, varDesc->lpvarValue, 0, VT_BSTR);

    if(hr) 
    {
            oss<< L"???";
    }
    else 
    {
        oss<< _wcsdup(variant.bstrVal);
    }
    return oss.str();
}
/*--------------------------------------------------------------------------*/
