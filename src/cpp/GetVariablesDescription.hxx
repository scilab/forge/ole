/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <windows.h>
#include <string>
#include <sstream>
#include <Ole2.h>
#include <OAIdl.h>
#include <comdef.h>
#include <OleAuto.h>
/*--------------------------------------------------------------------------*/
std::wstring stringifyVarDesc(VARDESC* varDesc, ITypeInfo* pti);
/*--------------------------------------------------------------------------*/
