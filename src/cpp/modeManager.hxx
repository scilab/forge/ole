/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#ifndef __MODEMANAGER_HXX__
#define __MODEMANAGER_HXX__

bool getVariantModeConversion(void);
void setVariantModeConversion(bool newVariantModeConversion);

#endif /* __MODEMANAGER_HXX__ */