/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/

#ifndef __AUTOMATIONHELPER_HXX__
#define __AUTOMATIONHELPER_HXX__

#include <windows.h>
#include <string>

enum INVOKE_ERROR {
	INVOKE_NO_ERROR = 0,
	INVOKE_NULL_IDISPATCH = 1,
	INVOKE_GETIDSOFNAMES = 2,
	INVOKE_BADPARAMCOUNT = 3,
	INVOKE_BADVARTYPE = 4,
	INVOKE_EXCEPTION = 5,
	INVOKE_MEMBERNOTFOUND = 6, 
	INVOKE_NONAMEDARGS = 7,
	INVOKE_OVERFLOW = 8,
	INVOKE_PARAMNOTFOUND = 9, 
	INVOKE_TYPEMISMATCH = 10,
	INVOKE_UNKNOWNINTERFACE = 11,
	INVOKE_UNKNOWNLCID = 12,
	INVOKE_PARAMNOTOPTIONAL = 13
};


/**
 * @brief Automation helper function
 */
INVOKE_ERROR ole_invoke(int autoType, VARIANT *pvResult, IDispatch *pDisp, LPOLESTR ptName, int cArgs, VARIANT* pvArgs);

wchar_t ** getMethodsName(IDispatch *pDisp, int *nbReturnedElements);
wchar_t ** getPropertyGetsName(IDispatch *pDisp, int *nbReturnedElements);
wchar_t ** getPropertyPutsName(IDispatch *pDisp, int *nbReturnedElements);
wchar_t ** getPropertiesName(IDispatch *pDisp, int *nbReturnedElements);


BOOL ole_isMethod(IDispatch *pDisp, const wchar_t* methodToSearch);
BOOL ole_isPropertyGet(IDispatch *pDisp, const wchar_t* propertyToSearch);
BOOL ole_isPropertyPut(IDispatch *pDisp, const wchar_t* propertyToSearch);

#endif /* __AUTOMATIONHELPER_HXX__ */
/*--------------------------------------------------------------------------*/
