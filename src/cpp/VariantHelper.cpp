/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#define _WIN32_WINNT 0x0501
#define _WIN32_IE 0x0501
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include <Windows.h>
#include <float.h>
#include <math.h>
#include <limits>
#include "VariantHelper.hxx"
#include "VariantsManager.hxx"
/*--------------------------------------------------------------------------*/
#define DIM_MAX 2
/*--------------------------------------------------------------------------*/
typedef enum { 
	ExcelErrDiv0  = -2146826281,
	ExcelErrNA    = -2146826246,
	ExcelErrName  = -2146826259,
	ExcelErrNull  = -2146826288,
	ExcelErrNum   = -2146826252,
	ExcelErrRef   = -2146826265,
	ExcelErrValue = -2146826273
} ExcelErrEnum;
/*--------------------------------------------------------------------------*/
static BOOL GetDimensions(SAFEARRAY* pSafeArray, int *m, int *n);
/*--------------------------------------------------------------------------*/
bool isVariant(void *pVariant)
{
	__try
	{
		VARIANT *pVar = (VARIANT*) pVariant;
		return findVariantInManager(pVar);
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		return false;
	}
}
/*--------------------------------------------------------------------------*/
const wchar_t *getVariantTypeAsString(VARIANT * pVariant)
{
	if (pVariant == NULL) return NULL;
	switch(pVariant->vt)
	{
		case VT_EMPTY: return L"VT_EMPTY";
		case VT_NULL: return L"VT_NULL";
		case VT_I2: return L"VT_I2";
		case VT_I4: return L"VT_I4";
		case VT_R4: return L"VT_R4";
		case VT_R8: return L"VT_R8";
		case VT_CY: return L"VT_CY";
		case VT_DATE: return L"VT_DATE";
		case VT_BSTR: return L"VT_BSTR";
		case VT_DISPATCH: return L"VT_DISPATCH";
		case VT_ERROR: return L"VT_ERROR";
		case VT_BOOL: return L"VT_BOOL";
		case VT_VARIANT: return L"VT_VARIANT";
		case VT_UNKNOWN: return L"VT_UNKNOWN";
		case VT_DECIMAL: return L"VT_DECIMAL";
		case VT_I1: return L"VT_I1";
		case VT_UI1: return L"VT_UI1";
		case VT_UI2: return L"VT_UI2";
		case VT_UI4: return L"VT_UI4";
		case VT_I8: return L"VT_I8";
		case VT_UI8: return L"VT_UI8";
		case VT_INT: return L"VT_INT";
		case VT_UINT: return L"VT_UINT";
		case VT_VOID: return L"VT_VOID";
		case VT_HRESULT: return L"VT_HRESULT";
		case VT_PTR: return L"VT_PTR";
		case VT_SAFEARRAY: return L"VT_SAFEARRAY";
		case VT_CARRAY: return L"VT_CARRAY";
		case VT_USERDEFINED: return L"VT_USERDEFINED";
		case VT_LPSTR: return L"VT_LPSTR";
		case VT_LPWSTR: return L"VT_LPWSTR";
		case VT_RECORD: return L"VT_RECORD";
		case VT_INT_PTR: return L"VT_INT_PTR";
		case VT_UINT_PTR: return L"VT_UINT_PTR";
		case VT_FILETIME: return L"VT_FILETIME";
		case VT_BLOB: return L"VT_BLOB";
		case VT_STREAM: return L"VT_STREAM";
		case VT_STORAGE: return L"VT_STORAGE";
		case VT_STREAMED_OBJECT: return L"VT_STREAMED_OBJECT";
		case VT_STORED_OBJECT: return L"VT_STORED_OBJECT";
		case VT_BLOB_OBJECT: return L"VT_BLOB_OBJECT";
		case VT_CF: return L"VT_CF";
		case VT_CLSID: return L"VT_CLSID";
		case VT_VERSIONED_STREAM: return L"VT_VERSIONED_STREAM";
		case VT_BSTR_BLOB: return L"VT_BSTR_BLOB";
		case VT_VECTOR: return L"VT_VECTOR";
		case VT_ARRAY: return L"VT_ARRAY";
		case VT_BYREF: return L"VT_BYREF";
		case VT_RESERVED: return L"VT_RESERVED";
		case VT_ILLEGAL: return L"VT_ILLEGAL";
	}
	return L"VT_UNKOWN";
}
/*--------------------------------------------------------------------------*/
VARIANT *doubleToVariant(double dValue, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_R8;
		varReturned->dblVal = dValue;
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *wstringToVariant(const wchar_t *pString, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_BSTR;
		varReturned->bstrVal = SysAllocString(pString);
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *int8ToVariant(char int8Value, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_I1;
        varReturned->cVal = int8Value;
        *iError = 0;
    }
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *unsignedint8ToVariant(unsigned char unsignedint8Value, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_UI1;
        varReturned->bVal = unsignedint8Value;
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *int16ToVariant(short int16Value, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_I2;
        varReturned->iVal = int16Value;
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *unsignedint16ToVariant(unsigned short unsignedint16Value, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_UI2;
        varReturned->uiVal = unsignedint16Value;
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *unsignedint32ToVariant(unsigned int unsignedint32Value, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_UINT;
        varReturned->uintVal = unsignedint32Value;
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *int32ToVariant(int iValue, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_INT;
		varReturned->intVal = iValue;
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *boolToVariant(BOOL bValue, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_BOOL;
		varReturned->boolVal =  (bValue == TRUE) ? VARIANT_TRUE : VARIANT_FALSE;
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
SAFEARRAY *matrixOfDoubleToSafearray(double *dValues, int rows, int cols, int *iError)
{
	SAFEARRAY *returnedArray = NULL;
	*iError = 1;
	if (dValues)
	{
		SAFEARRAYBOUND sab[2];
		sab[0].lLbound = 1;
		sab[0].cElements = rows;

		sab[1].lLbound = 1;
		sab[1].cElements = cols;

		returnedArray = SafeArrayCreate(VT_R8, 2, sab);
		if (returnedArray)
		{
			for(int j = 1; j <= cols; j++) 
			{
				for(int i = 1; i <= rows; i++) 
				{
					// Create entry value for (i,j)
					double dValue = dValues[(i-1) +(j-1) * rows];

					// Add to safearray...
					long indices[] = {i,j};
					if (SafeArrayPutElement(returnedArray, indices, (void *)&dValue) != S_OK)
					{
						SafeArrayDestroy(returnedArray);
						returnedArray = NULL;
						return NULL;
					}
				}
			}
			*iError = 0;
		}
	}
	return returnedArray;
}
/*--------------------------------------------------------------------------*/
SAFEARRAY *matrixOfDoubleToSafearrayVariant(double *dValues, int rows, int cols, int *iError)
{
	SAFEARRAY *returnedArray = NULL;
	*iError = 1;
	if (dValues)
	{
		SAFEARRAYBOUND sab[2];
		sab[0].lLbound = 1;
		sab[0].cElements = rows;

		sab[1].lLbound = 1;
		sab[1].cElements = cols;

		returnedArray = SafeArrayCreate(VT_VARIANT, 2, sab);
		if (returnedArray)
		{
			for(int j = 1; j <= cols; j++) 
			{
				for(int i = 1; i <= rows; i++) 
				{
					// Create entry value for (i,j)
					double dValue = dValues[(i-1) +(j-1) * rows];
					VARIANT* vValue = doubleToVariant(dValue, iError);

					// Add to safearray...
					long indices[] = {i,j};
					if (SafeArrayPutElement(returnedArray, indices, (void *)vValue) != S_OK)
					{
						SafeArrayDestroy(returnedArray);
						returnedArray = NULL;
						return NULL;
					}
				}
			}
			*iError = 0;
		}
	}
	return returnedArray;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfDoubleToVariant(double *dValues, int rows, int cols, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_VARIANT | VT_R8;
		varReturned->parray = matrixOfDoubleToSafearray(dValues, rows, cols, iError);
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfDoubleToVariantArray(double *dValues, int rows, int cols, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_ARRAY | VT_VARIANT;
		varReturned->parray = matrixOfDoubleToSafearrayVariant(dValues, rows, cols, iError);
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfWideStringToVariant(const wchar_t **pString, int rows, int cols, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_SAFEARRAY;

		SAFEARRAYBOUND sab[2];
		sab[0].lLbound = 1;
		sab[0].cElements = rows;

		sab[1].lLbound = 1;
		sab[1].cElements = cols;
		
		varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

		for(int j = 1; j<= cols; j++) 
		{
			for(int i = 1; i <= rows; i++) 
			{
				// Create entry value for (i,j)
				const wchar_t *Str = pString[(i-1) +(j-1)*rows];
				VARIANT tmp;
				VariantInit(&tmp);
				if (Str == NULL) 
				{
					tmp.vt = VT_EMPTY;
				}
				else
				{
					tmp.vt = VT_BSTR;
					tmp.bstrVal = SysAllocString(Str);
				}
				// Add to safearray...
				long indices[] = {i,j};
				SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
			}
		}
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfInt8ToVariant(char *int8Values, int rows, int cols, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_SAFEARRAY;

        SAFEARRAYBOUND sab[2];
        sab[0].lLbound = 1;
        sab[0].cElements = rows;

        sab[1].lLbound = 1;
        sab[1].cElements = cols;

        varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

        for(int j = 1; j <= cols; j++) 
        {
            for(int i = 1; i <= rows; i++) 
            {
                // Create entry value for (i,j)
                char pInt8Value = int8Values[(i-1) +(j-1) * rows];

                VARIANT tmp;
                VariantInit(&tmp);

                if (_isnan(pInt8Value))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNA;
                }
                else if (!_finite(pInt8Value))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNum;
                }
                else
                {
                    tmp.vt = VT_I1;
                    tmp.cVal = (CHAR)pInt8Value;
                }

                // Add to safearray...
                long indices[] = {i,j};
                SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
            }
        }
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfUnsignedInt8ToVariant(unsigned char *unsignedint8Values, int rows, int cols, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_SAFEARRAY;

        SAFEARRAYBOUND sab[2];
        sab[0].lLbound = 1;
        sab[0].cElements = rows;

        sab[1].lLbound = 1;
        sab[1].cElements = cols;

        varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

        for(int j = 1; j <= cols; j++) 
        {
            for(int i = 1; i <= rows; i++) 
            {
                // Create entry value for (i,j)
                unsigned char piValue = unsignedint8Values[(i-1) +(j-1) * rows];

                VARIANT tmp;
                VariantInit(&tmp);

                if (_isnan(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNA;
                }
                else if (!_finite(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNum;
                }
                else
                {
                    tmp.vt = VT_UI1;
                    tmp.bVal = piValue;
                }

                // Add to safearray...
                long indices[] = {i,j};
                SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
            }
        }
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfInt16ToVariant(short *int16Values, int rows, int cols, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_SAFEARRAY;

        SAFEARRAYBOUND sab[2];
        sab[0].lLbound = 1;
        sab[0].cElements = rows;

        sab[1].lLbound = 1;
        sab[1].cElements = cols;

        varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

        for(int j = 1; j <= cols; j++) 
        {
            for(int i = 1; i <= rows; i++) 
            {
                // Create entry value for (i,j)
                short piValue = int16Values[(i-1) +(j-1) * rows];

                VARIANT tmp;
                VariantInit(&tmp);

                if (_isnan(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNA;
                }
                else if (!_finite(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNum;
                }
                else
                {
                    tmp.vt = VT_I2;
                    tmp.iVal = piValue;
                }

                // Add to safearray...
                long indices[] = {i,j};
                SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
            }
        }
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfUnsignedInt16ToVariant(unsigned short *unsignedint16Values, int rows, int cols, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_SAFEARRAY;

        SAFEARRAYBOUND sab[2];
        sab[0].lLbound = 1;
        sab[0].cElements = rows;

        sab[1].lLbound = 1;
        sab[1].cElements = cols;

        varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

        for(int j = 1; j <= cols; j++) 
        {
            for(int i = 1; i <= rows; i++) 
            {
                // Create entry value for (i,j)
                unsigned short piValue = unsignedint16Values[(i-1) +(j-1) * rows];

                VARIANT tmp;
                VariantInit(&tmp);

                if (_isnan(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNA;
                }
                else if (!_finite(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNum;
                }
                else
                {
                    tmp.vt = VT_UI2;
                    tmp.uiVal = piValue;
                }

                // Add to safearray...
                long indices[] = {i,j};
                SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
            }
        }
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfUnsignedInt32ToVariant(unsigned int *unsignedint32Values, int rows, int cols, int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_SAFEARRAY;

        SAFEARRAYBOUND sab[2];
        sab[0].lLbound = 1;
        sab[0].cElements = rows;

        sab[1].lLbound = 1;
        sab[1].cElements = cols;

        varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

        for(int j = 1; j <= cols; j++) 
        {
            for(int i = 1; i <= rows; i++) 
            {
                // Create entry value for (i,j)
                unsigned int piValue = unsignedint32Values[(i-1) +(j-1) * rows];

                VARIANT tmp;
                VariantInit(&tmp);

                if (_isnan(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNA;
                }
                else if (!_finite(piValue))
                {
                    tmp.vt = VT_ERROR;
                    tmp.scode = ExcelErrNum;
                }
                else
                {
                    tmp.vt = VT_UINT;
                    tmp.uintVal = piValue;
                }

                // Add to safearray...
                long indices[] = {i,j};
                SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
            }
        }
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfInt32ToVariant(int *iValues, int rows, int cols, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_SAFEARRAY;

		SAFEARRAYBOUND sab[2];
		sab[0].lLbound = 1;
		sab[0].cElements = rows;

		sab[1].lLbound = 1;
		sab[1].cElements = cols;

		varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

		for(int j = 1; j <= cols; j++) 
		{
			for(int i = 1; i <= rows; i++) 
			{
				// Create entry value for (i,j)
				int piValue = iValues[(i-1) +(j-1) * rows];

				VARIANT tmp;
				VariantInit(&tmp);

				if (_isnan(piValue))
				{
					tmp.vt = VT_ERROR;
					tmp.scode = ExcelErrNA;
				}
				else if (!_finite(piValue))
				{
					tmp.vt = VT_ERROR;
					tmp.scode = ExcelErrNum;
				}
				else
				{
					tmp.vt = VT_INT;
					tmp.intVal = piValue;
				}

				// Add to safearray...
				long indices[] = {i,j};
				SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
			}
		}
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *matrixOfBoolToVariant(BOOL *bValues, int rows, int cols, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		
		varReturned->vt = VT_SAFEARRAY;

		SAFEARRAYBOUND sab[2];
		sab[0].lLbound = 1;
		sab[0].cElements = rows;
		
		sab[1].lLbound = 1;
		sab[1].cElements = cols;
		varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

		for(int j = 1; j<= cols; j++) 
		{
			for(int i = 1; i <= rows; i++) 
			{
				// Create entry value for (i,j)
				BOOL pbValue = bValues[(i-1) +(j-1)*rows];

				VARIANT tmp;
				VariantInit(&tmp);
				tmp.vt = VT_BOOL;
				tmp.boolVal = (pbValue == TRUE) ? VARIANT_TRUE : VARIANT_FALSE;

				// Add to safearray...
				long indices[] = {i,j};
				SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
			}
		}
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *mixedMatrixToVariant(mixedMatrix cell, int *iError)
{
	VARIANT *varReturned = NULL;
	*iError = 1;
	varReturned = new VARIANT;
	if (varReturned)
	{
		VariantInit(varReturned);
		VariantClear(varReturned);
		varReturned->vt = VT_SAFEARRAY;

		SAFEARRAYBOUND sab[2];
		sab[0].lLbound = 1;
		sab[0].cElements = cell.rows;

		sab[1].lLbound = 1;
		sab[1].cElements = cell.cols;

		varReturned->parray = SafeArrayCreate(VT_VARIANT, 2, sab);

		for(int j = 1; j <= cell.cols; j++) 
		{
			for(int i = 1; i <= cell.rows; i++) 
			{
				// Create entry value for (i,j)
				double pdValue = cell.dValues[(i-1) +(j-1) * cell.rows];
				wchar_t *pWstr = cell.pWstrs[(i-1) +(j-1) * cell.rows];

				VARIANT tmp;
				VariantInit(&tmp);
				
				// cell.pWstrs
				// "" toto "" 
				// NAN "" ""
				// "" tutu ""

				// cell.dValues
				// Nan 0 4
				// Nan 8 -Inf
				// 3  Nan Inf

				// VARIANT
				// VT_EMPTY VT_BSTR VT_R8
				// VT_ERROR VT_R8 VT_ERROR
				// VT_R8 VT_BSTR VT_ERROR




				if ((wcscmp(pWstr, L"") == 0) && (_isnan(pdValue)))
				{
					tmp.vt = VT_EMPTY;
				}

				if ((wcscmp(pWstr, L"") != 0) && (_isnan(pdValue)))
				{
					tmp.vt = VT_ERROR;
					tmp.scode = ExcelErrNA;
				}

/*
				if (_isnan(pdValue))
				{
					tmp.vt = VT_ERROR;
					tmp.scode = ExcelErrNA;
				}
				else if (!_finite(pdValue))
				{
					tmp.vt = VT_ERROR;
					tmp.scode = ExcelErrNum;
				}
				else
				{
					tmp.vt = VT_R8;
					tmp.dblVal = (DOUBLE)pdValue;
				}
*/
				// Add to safearray...
				long indices[] = {i,j};
				SafeArrayPutElement(varReturned->parray, indices, (void *)&tmp);
			}
		}
		*iError = 0;
	}
	return varReturned;
}
/*--------------------------------------------------------------------------*/
VARIANT *emptyToVariant(int *iError)
{
    VARIANT *varReturned = NULL;
    *iError = 1;
    varReturned = new VARIANT;
    if (varReturned)
    {
        VariantInit(varReturned);
        VariantClear(varReturned);
        varReturned->vt = VT_EMPTY;
        *iError = 0;
    }
    return varReturned;
}
/*--------------------------------------------------------------------------*/
double variantToDouble(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
	double dValue = 0.;
	*iError = 1;
	if (pvar->vt == VT_R8)
	{
		dValue = pvar->dblVal;
		*iError = 0;
	}
	else
	{
		VARIANT variantConverted;
		VariantInit(&variantConverted);
		if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_R8)))
		{
			dValue = variantConverted.dblVal;
			*iError = 0;
		}
		else
		{
			if (bConvertToNan)
			{
				dValue = std::numeric_limits<double>::quiet_NaN();
				*iError = 0;
			}
		}
	}
	return dValue;
}
/*--------------------------------------------------------------------------*/
wchar_t *variantToWideString(VARIANT *pvar, int *iError)
{
	wchar_t *pStrReturned = NULL;
	*iError = 1;
	if (pvar->vt == VT_BSTR)
	{
        if (pvar->bstrVal)
        {
            pStrReturned = _wcsdup(pvar->bstrVal);
        }
        else
        {
            pStrReturned = _wcsdup(L"");
        }
		*iError = 0;
	}
	else
	{
		VARIANT variantConverted;
		VariantInit(&variantConverted);
		if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_BSTR)))
		{
			pStrReturned = _wcsdup(variantConverted.bstrVal);
			*iError = 0;
		}
	}
	return pStrReturned;
}
/*--------------------------------------------------------------------------*/
int variantToInt32(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
	int iValue = 0;
	*iError = 1;
	if (pvar->vt == VT_INT)
	{
		iValue = pvar->intVal;
		*iError = 0;
	}
	else
	{
		VARIANT variantConverted;
		VariantInit(&variantConverted);
		if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_INT)))
		{
			iValue = variantConverted.intVal;
			*iError = 0;
		}
		else
		{
			if (bConvertToNan)
			{
				iValue = std::numeric_limits<int>::quiet_NaN();
				*iError = 0;
			}
		}
	}
	return iValue;
}
/*--------------------------------------------------------------------------*/
unsigned int variantToUnsignedInt32(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
    unsigned int unsignedint32Value = 0;
    *iError = 1;
    if (pvar->vt == VT_UINT)
    {
        unsignedint32Value = pvar->uintVal;
        *iError = 0;
    }
    else
    {
        VARIANT variantConverted;
        VariantInit(&variantConverted);
        if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_UINT)))
        {
            unsignedint32Value = variantConverted.uintVal;
            *iError = 0;
        }
        else
        {
            if (bConvertToNan)
            {
                 unsignedint32Value = std::numeric_limits<unsigned int>::quiet_NaN();
                *iError = 0;
            }
        }
    }
    return unsignedint32Value;
}
/*--------------------------------------------------------------------------*/
char variantToInt8(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
    char int8 = 0;
    *iError = 1;
    if (pvar->vt == VT_I1)
    {
        int8 = pvar->cVal;
        *iError = 0;
    }
    else
    {
        VARIANT variantConverted;
        VariantInit(&variantConverted);
        if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_I1)))
        {
            int8 = variantConverted.cVal;
            *iError = 0;
        }
        else
        {
            if (bConvertToNan)
            {
                int8 = std::numeric_limits<char>::quiet_NaN();
                *iError = 0;
            }
        }
    }
    return int8;
}
/*--------------------------------------------------------------------------*/
unsigned char variantToUnsignedInt8(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
    unsigned char uint8 = 0;
    *iError = 1;
    if (pvar->vt == VT_UI1)
    {
        uint8 = pvar->bVal;
        *iError = 0;
    }
    else
    {
        VARIANT variantConverted;
        VariantInit(&variantConverted);
        if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_UI1)))
        {
            uint8 = variantConverted.bVal;
            *iError = 0;
        }
        else
        {
            if (bConvertToNan)
            {
                uint8 = std::numeric_limits<unsigned char>::quiet_NaN();
                *iError = 0;
            }
        }
    }
    return uint8;
}
/*--------------------------------------------------------------------------*/
short variantToInt16(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
    short int16 = 0;
    *iError = 1;
    if (pvar->vt == VT_I2)
    {
        int16 = pvar->iVal;
        *iError = 0;
    }
    else
    {
        VARIANT variantConverted;
        VariantInit(&variantConverted);
        if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_I2)))
        {
            int16 = variantConverted.iVal;
            *iError = 0;
        }
        else
        {
            if (bConvertToNan)
            {
                int16 = std::numeric_limits<short>::quiet_NaN();
                *iError = 0;
            }
        }
    }
    return int16;
}
/*--------------------------------------------------------------------------*/
unsigned short variantToUnsignedInt16(VARIANT *pvar, BOOL bConvertToNan, int *iError)
{
    unsigned short uint16 = 0;
    *iError = 1;
    if (pvar->vt == VT_UI2)
    {
        uint16 = pvar->uiVal;
        *iError = 0;
    }
    else
    {
        VARIANT variantConverted;
        VariantInit(&variantConverted);
        if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_UI2)))
        {
            uint16 = variantConverted.uiVal;
            *iError = 0;
        }
        else
        {
            if (bConvertToNan)
            {
                uint16 = std::numeric_limits<unsigned short>::quiet_NaN();
                *iError = 0;
            }
        }
    }
    return uint16;
}
/*--------------------------------------------------------------------------*/
BOOL variantToBool(VARIANT *pvar, int *iError)
{
	BOOL bValue = FALSE;
	*iError = 1;
	if (pvar->vt == VT_BOOL)
	{
		 bValue = (pvar->boolVal == VARIANT_TRUE) ? TRUE : FALSE;
		 *iError = 0;
	}
	else
	{
		VARIANT variantConverted;
		VariantInit(&variantConverted);
		if (SUCCEEDED(VariantChangeType(&variantConverted, pvar, VARIANT_NOUSEROVERRIDE,VT_BOOL)))
		{
			bValue = (variantConverted.boolVal == VARIANT_TRUE) ? TRUE : FALSE;
			*iError = 0;
		}
	}
	return bValue;
}
/*--------------------------------------------------------------------------*/
double *safeArrayToMatrixOfDouble(SAFEARRAY *sfa, int *rows, int *cols, int *iError)
{
	double *dValues = NULL;
	*iError = 1;
	*rows = -1;
	*cols = -1;

	if (sfa)
	{
		int m = 0;
		int n = 0;
		if (GetDimensions(sfa, &m, &n))
		{
			int k = 0;
			dValues = new double[m * n];
			for(int j = 1; j <= m; j++) 
			{
				for(int i = 1; i <= n; i++) 
				{
					// Create entry value for (i,j)
					double dValue = 0.;

					// Get from safearray...
					long indices[] = {i,j};
					if (SafeArrayGetElement(sfa, indices, (void *)&dValue) != S_OK)
					{
						delete [] dValues;
						return NULL;
					}

					dValues[k] = dValue;
				}
			}

			*rows = m;
			*cols = n;
			*iError = 0;
		}
	}
	return dValues; 
}
/*--------------------------------------------------------------------------*/
double *variantToMatrixOfDouble(VARIANT *pvar, int* /*rows*/, int* /*cols*/, int *iError)
{
	double *dValues = NULL;
	*iError = 1;

	if (pvar)
	{
		int m = 0;
		int n = 0;
		SAFEARRAY* pSafeArray  = pvar->parray;
		dValues = safeArrayToMatrixOfDouble(pSafeArray, &m, &n, iError);
	}
	return dValues;
}
/*--------------------------------------------------------------------------*/
wchar_t **variantToMatrixOfWideString(VARIANT *pvar, int *rows, int *cols, int *iError)
{
	wchar_t **pStrs = NULL;
	*iError = 1;
	if (pvar)
	{
		int m = 0;
		int n = 0;
		SAFEARRAY* pSafeArray  = pvar->parray;
		if (GetDimensions(pSafeArray, &m, &n))
		{
			pStrs = new wchar_t *[m * n];
			if (pStrs)
			{
				int k = 0;
				for (int i = 1 ; i <= n; i++)
				{
					for (int j = 1; j <= m; j++)
					{
						HRESULT hRes;
						VARIANT CurrentChamp;
						long lDimension[DIM_MAX];

						lDimension[0] = j;
						lDimension[1] = i;
						hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
						if (SUCCEEDED(hRes))
						{
							if (CurrentChamp.vt != VT_BSTR)
							{
								hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_BSTR);
								if (SUCCEEDED(hRes))
								{
									pStrs[k] = _wcsdup(CurrentChamp.bstrVal);
									k++;
								}
								else
								{
									for (int l = 0; l < m * n; l++)
									{
										delete pStrs[l];
										pStrs = NULL;
									}
									delete [] pStrs;
									pStrs = NULL;

									*rows = -1;
									*cols = -1;
									*iError = 1;
									return NULL;
								}
							}
							else
							{
                                if (CurrentChamp.bstrVal)
                                {
                                    pStrs[k] = _wcsdup(CurrentChamp.bstrVal);
                                }
                                else
                                {
                                    pStrs[k] = _wcsdup(L"");
                                }
								k++;
							}
							VariantClear(&CurrentChamp);
						}
                        else
                        {
                            for (int l = 0; l < m * n; l++)
                            {
                                delete pStrs[l];
                                pStrs = NULL;
                            }
                            delete [] pStrs;
                            pStrs = NULL;

                            *rows = -1;
                            *cols = -1;
                            *iError = 1;
                            return NULL;
                        }
					}
				}

				*rows = m;
				*cols = n;
				*iError = 0;
			}
			else
			{
				*rows = -1;
				*cols = -1;
				*iError = 1;
				return NULL;
			}
		}
		else
		{
			*rows = -1;
			*cols = -1;
			*iError = 1;
			return NULL;
		}
	}
	return pStrs;
}
/*--------------------------------------------------------------------------*/
int *variantToMatrixOfInt32(VARIANT *pvar, int *rows, int *cols, int *iError)
{
	int *iValues = NULL;
	*iError = 1;

	if (pvar)
	{
		int m = 0;
		int n = 0;
		SAFEARRAY* pSafeArray  = pvar->parray;
		if (GetDimensions(pSafeArray, &m, &n))
		{
			iValues = new int[m * n];
			if (iValues)
			{
				int k = 0;
				for (int i = 1 ; i <= n; i++)
				{
					for (int j = 1; j <= m; j++)
					{
						HRESULT hRes;
						VARIANT CurrentChamp;
						long lDimension[DIM_MAX];

						lDimension[0] = j;
						lDimension[1] = i;
						hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
						if (SUCCEEDED(hRes))
						{
							if (CurrentChamp.vt != VT_INT)
							{
								hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_INT);
								if (SUCCEEDED(hRes))
								{
									iValues[k] = CurrentChamp.intVal;
									k++;
								}
								else
								{
									delete [] iValues;
									iValues = NULL;

									*rows = -1;
									*cols = -1;
									*iError = 1;
									return NULL;
								}
							}
							else
							{
								iValues[k] = CurrentChamp.intVal;
								k++;
							}
						}
						else
						{
							delete [] iValues;
							iValues = NULL;

							*rows = -1;
							*cols = -1;
							*iError = 1;
							return NULL;
						}
					}
				}

				*rows = m;
				*cols = n;
				*iError = 0;
			}
			else
			{
				*rows = -1;
				*cols = -1;
				*iError = 1;
				return NULL;
			}
		}
		else
		{
			*rows = -1;
			*cols = -1;
			*iError = 1;
			return NULL;
		}
	}
	else
	{
		*rows = -1;
		*cols = -1;
		*iError = 1;
		return NULL;
	}

	return iValues;
}
/*--------------------------------------------------------------------------*/
BOOL *variantToMatrixOfBool(VARIANT *pvar, int *rows, int *cols, int *iError)
{
	BOOL *bValues = NULL;
	*iError = 1;
	if (pvar->vt == VT_SAFEARRAY)
	{
		int m = 0;
		int n = 0;
		SAFEARRAY* pSafeArray  = pvar->parray;
		BOOL bDim = GetDimensions(pSafeArray, &m, &n);
		if (bDim)
		{
			bValues = new BOOL[m * n];
			if (bValues)
			{
				int k = 0;
				for (int i = 1 ; i <= n; i++)
				{
					for (int j = 1; j <= m; j++)
					{
						HRESULT hRes = S_FALSE;
						VARIANT CurrentChamp;
						long lDimension[DIM_MAX];

						lDimension[0] = j;
						lDimension[1] = i;
						hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
						if (SUCCEEDED(hRes))
						{
							if (CurrentChamp.vt != VT_BOOL)
							{
								hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_BOOL);
								if (SUCCEEDED(hRes))
								{
									bValues[k] = (CurrentChamp.boolVal == VARIANT_TRUE) ? TRUE : FALSE;
									k++;
								}
								else
								{
									delete [] bValues;
									bValues = NULL;

									*rows = -1;
									*cols = -1;
									*iError = 1;
									return NULL;
								}
							}
							else
							{
								bValues[k] = (CurrentChamp.boolVal == VARIANT_TRUE) ? TRUE : FALSE;
								k++;
							}
							VariantClear(&CurrentChamp);
						}
					}
				}
			}
			else
			{
				*rows = -1;
				*cols = -1;
				*iError = 1;
				return NULL;
			}
		}
		else
		{
			*rows = -1;
			*cols = -1;
			*iError = 1;
			return NULL;
		}
	}
	else
	{
		*rows = -1;
		*cols = -1;
		*iError = 1;
		return NULL;
	}
	return bValues;
}
/*--------------------------------------------------------------------------*/
static BOOL GetDimensions(SAFEARRAY* pSafeArray, int *m, int *n)
{
	UINT i = 0;
	UINT DimSafeArray = 0;
	UINT dimensions[DIM_MAX];

	DimSafeArray = SafeArrayGetDim(pSafeArray);

	/* We manage only array up to 2 dimensions */

	if (DimSafeArray <= DIM_MAX) 
	{
		for (i = 0;i < DimSafeArray;i++)
		{
			long lUpperBound = 0;
			long lLowerBound = 0;

			if (FAILED (SafeArrayGetLBound (pSafeArray,i+1,&lLowerBound)))
			{
				// error
				*m = -1;
				*n = -1;
				return FALSE;
			}
			if (FAILED (SafeArrayGetUBound (pSafeArray,i+1,&lUpperBound)))
			{
				// error
				*m = -1;
				*n = -1;
				return FALSE;
			}
			dimensions[i] = lUpperBound - lLowerBound + 1;
		}

		*m = dimensions[0];
		*n = dimensions[1];
		return TRUE;
	}
	return FALSE;
}
/*--------------------------------------------------------------------------*/
mixedMatrix * variantToMixedMatrix(VARIANT *pvar, int *iError)
{
	mixedMatrix *cell = NULL;
	*iError = 1;
    if (pvar == NULL) return cell;

	return cell;
}
/*--------------------------------------------------------------------------*/
SAFEARRAYTYPE getSafeArrayType(VARIANT *pvar)
{
	SAFEARRAYTYPE returnedType = SAFEARRAY_ERROR;
	if (pvar->vt == VT_SAFEARRAY) 
	{
		int m = 0;
		int n = 0;
		SAFEARRAY* pSafeArray  = pvar->parray;
		if (GetDimensions(pSafeArray, &m, &n))
		{
			VARTYPE previousVariant = VT_UNKNOWN;
			int k = 0;

			for (int i = 1 ; i <= m; i++)
			{
				for (int j = 1; j <= n; j++)
				{
					HRESULT hRes = S_FALSE;
					VARIANT CurrentChamp;
					long lDimension[DIM_MAX];

					lDimension[0] = i;
					lDimension[1] = j;
					hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp); 
					if FAILED(hRes) 
					{
						VariantClear(&CurrentChamp);
						return SAFEARRAY_ERROR;
					}

					if (k == 0)
					{
						previousVariant = CurrentChamp.vt;
					}
					else
					{
						if (previousVariant != CurrentChamp.vt)
						{
							VariantClear(&CurrentChamp);
							return SAFEARRAY_MIXED;
						}
						previousVariant = CurrentChamp.vt;
					}
				}
			}

			switch (previousVariant)
			{
			case VT_EMPTY: return SAFEARRAY_EMPTY;
			case VT_BOOL: return SAFEARRAY_BOOL;
			case VT_BSTR: return SAFEARRAY_WIDESTRING;
			case VT_R8: return SAFEARRAY_DOUBLE;
			case VT_INT: return SAFEARRAY_INT32;
			case VT_I1: return SAFEARRAY_INT8;
            case VT_I2: return SAFEARRAY_INT16;
			case VT_UI1: return SAFEARRAY_UINT8;
			case VT_UI2: return SAFEARRAY_UINT16;
            case VT_UINT: return SAFEARRAY_UINT32;
			case VT_UI4:
            case VT_R4:
            case VT_CY:
			case VT_I8:
			case VT_UI8:
				return SAFEARRAY_NEED_CONVERSION_DOUBLE;
				break;
			}
		}
		else
		{
			returnedType = SAFEARRAY_TYPE_NOT_MANAGED;
		}
	}
	else if (pvar->vt == VT_SAFEARRAYOFVARIANT)
    {
        returnedType = SAFEARRAY_VARIANT;
    }
    else
	{
		returnedType = SAFEARRAY_NOT_ARRAY;
	}

	return returnedType;
}
/*--------------------------------------------------------------------------*/
VARTYPE getVariantTypeFromString(const wchar_t *pStrVarType, int *iError)
{
	*iError = 1;

	if (pStrVarType == NULL)
	{
		*iError = 1;
		return VT_ERROR;
	}
	else if (wcscmp(L"VT_EMPTY", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_EMPTY;
	}
	else if (wcscmp(L"VT_NULL", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_NULL; 
	}
	else if (wcscmp(L"VT_I2", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_I2; 
	}
	else if (wcscmp(L"VT_I4", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_I4; 
	}
	else if (wcscmp(L"VT_R4", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_R4; 
	}
	else if (wcscmp(L"VT_R8", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_R8; 
	}
	else if (wcscmp(L"VT_CY", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_CY; 
	}
	else if (wcscmp(L"VT_DATE", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_DATE; 
	}
	else if (wcscmp(L"VT_BSTR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_BSTR; 
	}
	else if (wcscmp(L"VT_DISPATCH", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_DISPATCH; 
	}
	else if (wcscmp(L"VT_ERROR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_ERROR; 
	}
	else if (wcscmp(L"VT_BOOL", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_BOOL; 
	}
	else if (wcscmp(L"VT_VARIANT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_VARIANT; 
	}
	else if (wcscmp(L"VT_UNKNOWN", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UNKNOWN; 
	}
	else if (wcscmp(L"VT_DECIMAL", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_DECIMAL; 
	}
	else if (wcscmp(L"VT_I1", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_I1; 
	}
	else if (wcscmp(L"VT_UI1", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UI1; 
	}
	else if (wcscmp(L"VT_UI2", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UI2; 
	}
	else if (wcscmp(L"VT_UI4", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UI4; 
	}
	else if (wcscmp(L"VT_I8", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_I8; 
	}
	else if (wcscmp(L"VT_UI8", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UI8; 
	}
	else if (wcscmp(L"VT_INT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_INT; 
	}
	else if (wcscmp(L"VT_UINT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UINT; 
	}
	else if (wcscmp(L"VT_VOID", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_VOID; 
	}
	else if (wcscmp(L"VT_HRESULT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_HRESULT; 
	}
	else if (wcscmp(L"VT_PTR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_PTR; 
	}
	else if (wcscmp(L"VT_SAFEARRAY", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_SAFEARRAY; 
	}
	else if (wcscmp(L"VT_CARRAY", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_CARRAY;  
	}
	else if (wcscmp(L"VT_USERDEFINED", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_USERDEFINED;
	}
	else if (wcscmp(L"VT_LPSTR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_LPSTR; 
	}
	else if (wcscmp(L"VT_LPWSTR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_LPWSTR;
	}
	else if (wcscmp(L"VT_RECORD", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_RECORD;
	}
	else if (wcscmp(L"VT_INT_PTR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_INT_PTR;
	}
	else if (wcscmp(L"VT_UINT_PTR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_UINT_PTR;
	}
	else if (wcscmp(L"VT_FILETIME", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_FILETIME; 
	}
	else if (wcscmp(L"VT_BLOB", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_BLOB; 
	}
	else if (wcscmp(L"VT_STREAM", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_STREAM; 
	}
	else if (wcscmp(L"VT_STORAGE", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_STORAGE;
	}
	else if (wcscmp(L"VT_STREAMED_OBJECT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_STORAGE;
	}
	else if (wcscmp(L"VT_STORED_OBJECT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_STORED_OBJECT;
	}
	else if (wcscmp(L"VT_BLOB_OBJECT", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_BLOB_OBJECT; 
	}
	else if (wcscmp(L"VT_CF", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_CF; 
	}
	else if (wcscmp(L"VT_CLSID", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_CLSID; 
	}
	else if (wcscmp(L"VT_VERSIONED_STREAM", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_VERSIONED_STREAM; 
	}
	else if (wcscmp(L"VT_BSTR_BLOB", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_BSTR_BLOB; 
	}
	else if (wcscmp(L"VT_VECTOR", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_VECTOR; 
	}
	else if (wcscmp(L"VT_ARRAY", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_ARRAY; 
	}
	else if (wcscmp(L"VT_BYREF", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_BYREF; 
	}
	else if (wcscmp(L"VT_RESERVED", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_RESERVED; 
	}
	else if (wcscmp(L"VT_ILLEGAL", pStrVarType) == 0)
	{
		*iError = 0;
		return VT_ILLEGAL; 
	}

	*iError = 1;
	return VT_UNKNOWN;
}
/*--------------------------------------------------------------------------*/
char *variantToMatrixOfInt8(VARIANT *pvar, int *rows, int *cols, int *iError)
{
    char *int8Values = NULL;
    *iError = 1;

    if (pvar)
    {
        int m = 0;
        int n = 0;
        SAFEARRAY* pSafeArray  = pvar->parray;
        if (GetDimensions(pSafeArray, &m, &n))
        {
            int8Values = new char[m * n];
            if (int8Values)
            {
                int k = 0;
                for (int i = 1 ; i <= n; i++)
                {
                    for (int j = 1; j <= m; j++)
                    {
                        HRESULT hRes;
                        VARIANT CurrentChamp;
                        long lDimension[DIM_MAX];

                        lDimension[0] = j;
                        lDimension[1] = i;
                        hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
                        if (SUCCEEDED(hRes))
                        {
                            if (CurrentChamp.vt != VT_I1)
                            {
                                hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_I1);
                                if (SUCCEEDED(hRes))
                                {
                                    int8Values[k] = CurrentChamp.cVal;
                                    k++;
                                }
                                else
                                {
                                    delete [] int8Values;
                                    int8Values = NULL;

                                    *rows = -1;
                                    *cols = -1;
                                    *iError = 1;
                                    return NULL;
                                }
                            }
                            else
                            {
                                int8Values[k] = CurrentChamp.cVal;
                                k++;
                            }
                            VariantClear(&CurrentChamp);
                        }
                        else
                        {
                            delete [] int8Values;
                            int8Values = NULL;

                            *rows = -1;
                            *cols = -1;
                            *iError = 1;
                            return NULL;
                        }
                    }
                }

                *rows = m;
                *cols = n;
                *iError = 0;
            }
            else
            {
                *rows = -1;
                *cols = -1;
                *iError = 1;
                return NULL;
            }
        }
        else
        {
            *rows = -1;
            *cols = -1;
            *iError = 1;
            return NULL;
        }
    }
    return int8Values;
}
/*--------------------------------------------------------------------------*/
short *variantToMatrixOfInt16(VARIANT *pvar, int *rows, int *cols, int *iError)
{
    short *int16Values = NULL;
    *iError = 1;

    if (pvar)
    {
        int m = 0;
        int n = 0;
        SAFEARRAY* pSafeArray  = pvar->parray;
        if (GetDimensions(pSafeArray, &m, &n))
        {
            int16Values = new short[m * n];
            if (int16Values)
            {
                int k = 0;
                for (int i = 1 ; i <= n; i++)
                {
                    for (int j = 1; j <= m; j++)
                    {
                        HRESULT hRes;
                        VARIANT CurrentChamp;
                        long lDimension[DIM_MAX];

                        lDimension[0] = j;
                        lDimension[1] = i;
                        hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
                        if (SUCCEEDED(hRes))
                        {
                            if (CurrentChamp.vt != VT_I2)
                            {
                                hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_I2);
                                if (SUCCEEDED(hRes))
                                {
                                    int16Values[k] = CurrentChamp.iVal;
                                    k++;
                                }
                                else
                                {
                                    delete [] int16Values;
                                    int16Values = NULL;

                                    *rows = -1;
                                    *cols = -1;
                                    *iError = 1;
                                    return NULL;
                                }
                            }
                            else
                            {
                                int16Values[k] = CurrentChamp.iVal;
                                k++;
                            }
                            VariantClear(&CurrentChamp);
                        }
                        else
                        {
                            delete [] int16Values;
                            int16Values = NULL;

                            *rows = -1;
                            *cols = -1;
                            *iError = 1;
                            return NULL;
                        }
                    }
                }

                *rows = m;
                *cols = n;
                *iError = 0;
            }
            else
            {
                *rows = -1;
                *cols = -1;
                *iError = 1;
                return NULL;
            }
        }
        else
        {
            *rows = -1;
            *cols = -1;
            *iError = 1;
            return NULL;
        }
    }
    return int16Values;
}
/*--------------------------------------------------------------------------*/
unsigned char *variantToMatrixOfUnsignedInt8(VARIANT *pvar, int *rows, int *cols, int *iError)
{
    unsigned char *unsignedchar8Values = NULL;
    *iError = 1;

    if (pvar)
    {
        int m = 0;
        int n = 0;
        SAFEARRAY* pSafeArray  = pvar->parray;
        if (GetDimensions(pSafeArray, &m, &n))
        {
            unsignedchar8Values = new unsigned char[m * n];
            if (unsignedchar8Values)
            {
                int k = 0;
                for (int i = 1 ; i <= n; i++)
                {
                    for (int j = 1; j <= m; j++)
                    {
                        HRESULT hRes;
                        VARIANT CurrentChamp;
                        long lDimension[DIM_MAX];

                        lDimension[0] = j;
                        lDimension[1] = i;
                        hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
                        if (SUCCEEDED(hRes))
                        {
                            if (CurrentChamp.vt != VT_UI1)
                            {
                                hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_UI1);
                                if (SUCCEEDED(hRes))
                                {
                                    unsignedchar8Values[k] = CurrentChamp.bVal;
                                    k++;
                                }
                                else
                                {
                                    delete [] unsignedchar8Values;
                                    unsignedchar8Values = NULL;

                                    *rows = -1;
                                    *cols = -1;
                                    *iError = 1;
                                    return NULL;
                                }
                            }
                            else
                            {
                                unsignedchar8Values[k] = CurrentChamp.bVal;
                                k++;
                            }
                            VariantClear(&CurrentChamp);
                        }
                        else
                        {
                            delete [] unsignedchar8Values;
                            unsignedchar8Values = NULL;

                            *rows = -1;
                            *cols = -1;
                            *iError = 1;
                            return NULL;
                        }
                    }
                }

                *rows = m;
                *cols = n;
                *iError = 0;
            }
            else
            {
                *rows = -1;
                *cols = -1;
                *iError = 1;
                return NULL;
            }
        }
        else
        {
            *rows = -1;
            *cols = -1;
            *iError = 1;
            return NULL;
        }
    }
    return unsignedchar8Values;
}
/*--------------------------------------------------------------------------*/
unsigned short *variantToMatrixOfUnsignedInt16(VARIANT *pvar, int *rows, int *cols, int *iError)
{
    unsigned short *unsignedint16Values = NULL;
    *iError = 1;

    if (pvar)
    {
        int m = 0;
        int n = 0;
        SAFEARRAY* pSafeArray  = pvar->parray;
        if (GetDimensions(pSafeArray, &m, &n))
        {
            unsignedint16Values = new unsigned short[m * n];
            if (unsignedint16Values)
            {
                int k = 0;
                for (int i = 1 ; i <= n; i++)
                {
                    for (int j = 1; j <= m; j++)
                    {
                        HRESULT hRes;
                        VARIANT CurrentChamp;
                        long lDimension[DIM_MAX];

                        lDimension[0] = j;
                        lDimension[1] = i;
                        hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
                        if (SUCCEEDED(hRes))
                        {
                            if (CurrentChamp.vt != VT_UI2)
                            {
                                hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_UI2);
                                if (SUCCEEDED(hRes))
                                {
                                    unsignedint16Values[k] = CurrentChamp.uiVal;
                                    k++;
                                }
                                else
                                {
                                    delete [] unsignedint16Values;
                                    unsignedint16Values = NULL;

                                    *rows = -1;
                                    *cols = -1;
                                    *iError = 1;
                                    return NULL;
                                }
                            }
                            else
                            {
                                unsignedint16Values[k] = CurrentChamp.uiVal;
                                k++;
                            }
                            VariantClear(&CurrentChamp);
                        }
                        else
                        {
                            delete [] unsignedint16Values;
                            unsignedint16Values = NULL;

                            *rows = -1;
                            *cols = -1;
                            *iError = 1;
                            return NULL;
                        }
                    }
                }

                *rows = m;
                *cols = n;
                *iError = 0;
            }
            else
            {
                *rows = -1;
                *cols = -1;
                *iError = 1;
                return NULL;
            }
        }
        else
        {
            *rows = -1;
            *cols = -1;
            *iError = 1;
            return NULL;
        }
    }
    return unsignedint16Values;
}
/*--------------------------------------------------------------------------*/
unsigned int *variantToMatrixOfUnsignedInt32(VARIANT *pvar, int *rows, int *cols, int *iError)
{
    unsigned int *uint32Values = NULL;
    *iError = 1;

    if (pvar)
    {
        int m = 0;
        int n = 0;
        SAFEARRAY* pSafeArray  = pvar->parray;
        if (GetDimensions(pSafeArray, &m, &n))
        {
            uint32Values = new unsigned int[m * n];
            if (uint32Values)
            {
                int k = 0;
                for (int i = 1 ; i <= n; i++)
                {
                    for (int j = 1; j <= m; j++)
                    {
                        HRESULT hRes;
                        VARIANT CurrentChamp;
                        long lDimension[DIM_MAX];

                        lDimension[0] = j;
                        lDimension[1] = i;
                        hRes = SafeArrayGetElement(pSafeArray, lDimension, &CurrentChamp);
                        if (SUCCEEDED(hRes))
                        {
                            if (CurrentChamp.vt != VT_UINT)
                            {
                                hRes = VariantChangeType(&CurrentChamp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_UINT);
                                if (SUCCEEDED(hRes))
                                {
                                    uint32Values[k] = CurrentChamp.uintVal;
                                    k++;
                                }
                                else
                                {
                                    delete [] uint32Values;
                                    uint32Values = NULL;

                                    *rows = -1;
                                    *cols = -1;
                                    *iError = 1;
                                    return NULL;
                                }
                            }
                            else
                            {
                                uint32Values[k] = CurrentChamp.uintVal;
                                k++;
                            }
                        }
                        else
                        {
                            delete [] uint32Values;
                            uint32Values = NULL;

                            *rows = -1;
                            *cols = -1;
                            *iError = 1;
                            return NULL;
                        }
                    }
                }

                *rows = m;
                *cols = n;
                *iError = 0;
            }
            else
            {
                *rows = -1;
                *cols = -1;
                *iError = 1;
                return NULL;
            }
        }
        else
        {
            *rows = -1;
            *cols = -1;
            *iError = 1;
            return NULL;
        }
    }
    else
    {
        *rows = -1;
        *cols = -1;
        *iError = 1;
        return NULL;
    }

    return uint32Values;
}
/*--------------------------------------------------------------------------*/
 VARIANT *safeArrayOfVariantToSafeArrayOfWideString(VARIANT* /*pVariant*/, int *iError)
{
    VARIANT *pResult = NULL;
    *iError = 1;

    return pResult;
}
/*--------------------------------------------------------------------------*/
VARIANT * ArrayOfToSafeArray(VARIANT* /*pVariant*/, int *iError)
 {
     VARIANT *pResult = NULL;
     *iError = 1;

     return pResult;
 }
 /*--------------------------------------------------------------------------*/
int extractDataForMListFromArrayOfVariant(VARIANT *pVariant,
                                         int *rows, int *cols,
                                         wchar_t ***pwStrsValues,
                                         double **pdoublesValues)
{
    int iError = 1;
    if (pVariant)
    {
        SAFEARRAY *arr = pVariant->parray;
        int m = 0;
        int n = 0;

        if (GetDimensions(arr, &m, &n))
        {
			// handle 1-d arrays as 1xn or mx1
			if(n==0 && m>0) 
			{
				n = 1;
			}
			else 
			{
				if (m==0 && n>0) {
					m = 1;
				}
			}


            int iNbElements = m * n;
            *pdoublesValues = new double [iNbElements];
            *pwStrsValues = new wchar_t * [iNbElements];
            int k = 0;
            for (int i = 0 ; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    HRESULT hRes;
                    VARIANT CurrentChamp;
                    long lDimension[DIM_MAX];
                    lDimension[0] = j;
                    lDimension[1] = i;
                    VariantInit(&CurrentChamp);
                    hRes = SafeArrayGetElement(arr, lDimension, &CurrentChamp);
                    if (SUCCEEDED(hRes))
                    {
                        if (CurrentChamp.vt == VT_R8)
                        {
                            (*pdoublesValues)[k] = CurrentChamp.dblVal;
                            (*pwStrsValues)[k] = _wcsdup(L"");
                            k++;
                        }
                        else if (CurrentChamp.vt == VT_BSTR)
                        {
                            (*pdoublesValues)[k] = std::numeric_limits<double>::quiet_NaN();
                            (*pwStrsValues)[k] = _wcsdup(CurrentChamp.bstrVal);
                            k++;
                        }
                        else
                        {
                            VARIANT varTmp;

                            VariantInit(&varTmp);
                            hRes = VariantChangeType(&varTmp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_R8);
                            if (SUCCEEDED(hRes))
                            {
                                (*pdoublesValues)[k] = varTmp.dblVal;
                                (*pwStrsValues)[k] = _wcsdup(L"");
                                k++;
                            }
                            else
                            {
                                VariantInit(&varTmp);
                                hRes = VariantChangeType(&varTmp, &CurrentChamp, VARIANT_NOUSEROVERRIDE,VT_BSTR);
                                if (SUCCEEDED(hRes))
                                {
                                    (*pdoublesValues)[k] = std::numeric_limits<double>::quiet_NaN();
                                    (*pwStrsValues)[k] = _wcsdup(CurrentChamp.bstrVal);
                                    k++;
                                }
                                else
                                {
                                    (*pwStrsValues)[k] = _wcsdup(L"#ERROR#");
                                    (*pdoublesValues)[k] = 0.;
                                    k++;
                                }
                            }
                        }
                    }
                    else
                    {
                        (*pwStrsValues)[k] = _wcsdup(L"#ERROR#");
                        (*pdoublesValues)[k] = 0.;
                        k++;
                    }
                }
            }
            *rows = m;
            *cols = n;
            iError = 0;
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
HRESULT copyVariant(VARIANT *dstvar, VARIANT *srcvar)
{
	HRESULT ret = S_OK;

	if (dstvar)
	{
		switch (V_VT(srcvar)) 
		{
			case VT_EMPTY:
			case VT_NULL:
			case VT_VOID:
			case VT_UI1:
			case VT_I1:
			case VT_UI2:
			case VT_I2:
			case VT_UI4: 
			case VT_I4:
			case VT_INT:
			case VT_UINT:
			case VT_R4:
			case VT_R8:
			case VT_BOOL:
                memcpy((void*)dstvar,srcvar, sizeof(VARIANT));
                V_VT(dstvar) = V_VT(srcvar);
            break;
			case VT_ARRAY | VT_VARIANT:
			case VT_ARRAY | VT_R8:
				memcpy((void*)dstvar,srcvar, sizeof(VARIANT));
				ret = SafeArrayCopy(srcvar->parray, &dstvar->parray);
				V_VT(dstvar) = V_VT(srcvar);
				break;
			case VT_BSTR:
                dstvar->bstrVal = SysAllocString(srcvar->bstrVal);
                V_VT(dstvar) = V_VT(srcvar);
			break;
			case VT_UNKNOWN:
			case VT_DISPATCH:
			case VT_VARIANT:
                memcpy((void*)dstvar,srcvar, sizeof(VARIANT));
                V_VT(dstvar) = V_VT(srcvar);
				break;
			default:
				ret = S_FALSE;
				break;
		}
	}
    else
    {
        ret = S_FALSE;
    }
	return ret;
}
/*--------------------------------------------------------------------------*/
