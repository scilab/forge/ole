/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <comdef.h>
#include "AutomationHelper.hxx"
/*--------------------------------------------------------------------------*/
#define RANGE_SEPARATOR ':'
/*--------------------------------------------------------------------------*/
static wchar_t **getFunctionsName(IDispatch *pDisp, int *nbReturnedElements, INVOKEKIND wantedKind);
static BOOL ole_isFound(IDispatch *pDisp, const wchar_t* wnameToSearch, INVOKEKIND wantedKind);
/*--------------------------------------------------------------------------*/
/* qsort C-string comparison function */ 
static int cstring_cmp(const void *a, const void *b) 
{ 
    const wchar_t **ia = (const wchar_t **)a;
    const wchar_t **ib = (const wchar_t **)b;
    return wcscmp(*ia, *ib);
} 
/*--------------------------------------------------------------------------*/
INVOKE_ERROR ole_invoke(int autoType, VARIANT *pvResult, IDispatch *pDisp, LPOLESTR ptName, int cArgs, VARIANT *pvArgs)
{
	__try
	{
    // Begin variable-argument list...

    if(!pDisp)
    {
        return INVOKE_NULL_IDISPATCH;
    }

    // Variables used...
    DISPPARAMS dp = { NULL, NULL, 0, 0 };
    DISPID dispidNamed = DISPID_PROPERTYPUT;
    DISPID dispID;
    HRESULT hr;
    char szName[200];

    // Convert down to ANSI
    WideCharToMultiByte(CP_ACP, 0, ptName, -1, szName, 256, NULL, NULL);

    // Get DISPID for name passed...
    hr = pDisp->GetIDsOfNames(IID_NULL, &ptName, 1, LOCALE_USER_DEFAULT, &dispID);
    if(FAILED(hr))
    {
        return INVOKE_GETIDSOFNAMES;
    }

    // Build DISPPARAMS
    dp.cArgs = cArgs;
    dp.rgvarg = pvArgs;

    // Handle special-case for property-puts!
    if(autoType & DISPATCH_PROPERTYPUT)
    {
        dp.cNamedArgs = 1;
        dp.rgdispidNamedArgs = &dispidNamed;
    }

    // Make the call!
    hr = pDisp->Invoke(dispID, IID_NULL, LOCALE_SYSTEM_DEFAULT, (WORD)autoType, &dp, pvResult, NULL, NULL);

    if(FAILED(hr))
    {
        switch(hr)
        {
        case DISP_E_BADPARAMCOUNT:
            return INVOKE_BADPARAMCOUNT;
        case DISP_E_BADVARTYPE:
            return INVOKE_BADVARTYPE;
        case DISP_E_EXCEPTION:
            return INVOKE_EXCEPTION;
        case DISP_E_MEMBERNOTFOUND:
            return INVOKE_MEMBERNOTFOUND;
        case DISP_E_NONAMEDARGS:
            return INVOKE_NONAMEDARGS;
        case DISP_E_OVERFLOW:
            return INVOKE_OVERFLOW;
        case DISP_E_PARAMNOTFOUND:
            return INVOKE_PARAMNOTFOUND;
        case DISP_E_TYPEMISMATCH:
            return INVOKE_TYPEMISMATCH;
        case DISP_E_UNKNOWNINTERFACE:
            return INVOKE_UNKNOWNINTERFACE;
        case DISP_E_UNKNOWNLCID:
            return INVOKE_UNKNOWNLCID;
        case DISP_E_PARAMNOTOPTIONAL:
            return INVOKE_PARAMNOTOPTIONAL;
        }
     }
	}

	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		return INVOKE_EXCEPTION;
	}

    return INVOKE_NO_ERROR;
}
/*--------------------------------------------------------------------------*/
static wchar_t** getFunctionsName(IDispatch *pDisp, int *nbReturnedElements, INVOKEKIND wantedKind)
{
	int iSizeArrayReturned = 0;
	wchar_t** returnedFunctionNames = NULL;
	HRESULT hr = S_FALSE;
	ITypeInfo *pITypeInfo = NULL;

	__try
	{
		if (SUCCEEDED(pDisp->GetTypeInfo(0, LOCALE_USER_DEFAULT, &pITypeInfo)))
		{
			TYPEATTR *pTypeAttr = NULL;
			if (SUCCEEDED(hr = pITypeInfo->GetTypeAttr(&pTypeAttr)))
			{
				UINT iMaxFunc = pTypeAttr->cFuncs -1;
				for (UINT iFunc = 0; iFunc <= iMaxFunc; ++iFunc)
				{
					UINT iFuncDesc = iMaxFunc - iFunc;
					FUNCDESC *pFuncDesc = NULL;
					if (SUCCEEDED(hr = pITypeInfo->GetFuncDesc(iFuncDesc, &pFuncDesc)))
					{
						if (pFuncDesc->invkind &  wantedKind && !(pFuncDesc->wFuncFlags & 1))
						{
							BSTR bstrName;
							UINT cNames;
							if (SUCCEEDED(hr = pITypeInfo->GetNames(pFuncDesc->memid, &bstrName, 1, &cNames)))
							{
								iSizeArrayReturned++;
							}
						}
						pITypeInfo->ReleaseFuncDesc(pFuncDesc);
					}
				}

				returnedFunctionNames = new wchar_t*[iSizeArrayReturned];
				if (returnedFunctionNames)
				{
					int i = 0;
					for (UINT iFunc = 0; iFunc <= iMaxFunc; ++iFunc)
					{
						UINT iFuncDesc = iMaxFunc - iFunc;
						FUNCDESC *pFuncDesc = NULL;
						if (SUCCEEDED(hr = pITypeInfo->GetFuncDesc(iFuncDesc, &pFuncDesc)))
						{
							if (pFuncDesc->invkind &  wantedKind && !(pFuncDesc->wFuncFlags & 1))
							{
								BSTR bstrName;
								UINT cNames;
								if (SUCCEEDED(hr = pITypeInfo->GetNames(pFuncDesc->memid, &bstrName, 1, &cNames)))
								{
                                    returnedFunctionNames[i] = _wcsdup(bstrName);
									i++;
								}
							}
							pITypeInfo->ReleaseFuncDesc(pFuncDesc);
						}
						*nbReturnedElements = iSizeArrayReturned;
					}
				}
				pITypeInfo->ReleaseTypeAttr(pTypeAttr);
			}
			pITypeInfo->Release();
		}
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		returnedFunctionNames = NULL;
		*nbReturnedElements = 0;
	}
	return returnedFunctionNames;
}
/*--------------------------------------------------------------------------*/
static BOOL ole_isFound(IDispatch *pDisp, const wchar_t* wnameToSearch, INVOKEKIND wantedKind)
{
    wchar_t** tableNames = NULL;
    int sizeNames = 0;
    BOOL bFound = FALSE;

    if (wnameToSearch)
    {
        switch (wantedKind)
        {
        case INVOKE_FUNC:
            tableNames =  getFunctionsName(pDisp, &sizeNames, INVOKE_FUNC);
            break;
        case INVOKE_PROPERTYGET:
            tableNames = getFunctionsName(pDisp, &sizeNames, INVOKE_PROPERTYGET);
            break;
        case INVOKE_PROPERTYPUT:
            tableNames = getFunctionsName(pDisp, &sizeNames, INVOKE_PROPERTYPUT);
            break;
        }

        if (tableNames)
        {
            for (int i = 0; i < sizeNames; i++)
            {
                if (wcscmp(tableNames[i], wnameToSearch) == 0)
                {
                    bFound = TRUE;
                    break;
                }
            }

            for (int i = 0; i < sizeNames; i++)
            {
                if (tableNames[i])
                {
                    delete tableNames[i];
                    tableNames[i] = NULL;
                }
            }
            delete [] tableNames;
            tableNames = NULL;
        }
    }
    return bFound;
}
/*--------------------------------------------------------------------------*/
wchar_t ** getMethodsName(IDispatch *pDisp, int *nbReturnedElements)
{
	return getFunctionsName(pDisp, nbReturnedElements, INVOKE_FUNC);
}
/*--------------------------------------------------------------------------*/
wchar_t ** getPropertyGetsName(IDispatch *pDisp, int *nbReturnedElements)
{
	return getFunctionsName(pDisp, nbReturnedElements, INVOKE_PROPERTYGET);
}
/*--------------------------------------------------------------------------*/
wchar_t ** getPropertyPutsName(IDispatch *pDisp, int *nbReturnedElements)
{
	return getFunctionsName(pDisp, nbReturnedElements, INVOKE_PROPERTYPUT);
}
/*--------------------------------------------------------------------------*/
wchar_t ** getPropertiesName(IDispatch *pDisp, int *nbReturnedElements)
{
	int iSizeArrayReturned = 0;

	int iSizePropertyPut = 0;
	int iSizePropertyGet = 0;
	int iSizePropertyAll = 0;

	wchar_t** propertiesPut = NULL;
	wchar_t** propertiesGet = NULL;
	wchar_t** propertiesAll = NULL;

	wchar_t** returnedProperties = NULL;

	*nbReturnedElements = 0;

	propertiesGet = getPropertyGetsName(pDisp, &iSizePropertyGet);
	propertiesPut = getPropertyPutsName(pDisp, &iSizePropertyPut);
	
	iSizePropertyAll = iSizePropertyPut + iSizePropertyGet;
	propertiesAll = new wchar_t*[iSizePropertyAll];
	if (propertiesAll)
	{
		int k = 0;
		if (propertiesGet)
		{
			for (int i = 0; i < iSizePropertyGet ; i++)
			{
				propertiesAll[k] = propertiesGet[i];
				k++;
			}
			delete [] propertiesGet;
			propertiesGet = NULL;
		}

		if (propertiesPut)
		{
			for (int i = 0; i < iSizePropertyPut ; i++)
			{
				propertiesAll[k] = propertiesPut[i];
				k++;
			}
			delete [] propertiesPut;
			propertiesPut = NULL;
		}

		qsort(propertiesAll, iSizePropertyAll, sizeof(wchar_t *), cstring_cmp);

		wchar_t *previousProperty = NULL;
		iSizeArrayReturned = 0;
		for (int i = 0; i < iSizePropertyAll; i++)
		{
			if (i == 0)
			{
				previousProperty = _wcsdup(propertiesAll[i]);
				iSizeArrayReturned++;
			}
			else
			{
				if (wcscmp(propertiesAll[i], previousProperty) == 0)
				{
					delete propertiesAll[i];
					propertiesAll[i] = NULL;
				}
				else
				{
					delete previousProperty; 
					previousProperty = _wcsdup(propertiesAll[i]);
					iSizeArrayReturned++;
				}	
			}
		}

		if (previousProperty)
		{
			delete previousProperty;
			previousProperty = NULL;
		}

		returnedProperties = new wchar_t*[iSizeArrayReturned];
		if (returnedProperties)
		{
			int l = 0;
			for (int i = 0; i < iSizePropertyAll; i++)
			{
				if (propertiesAll[i])
				{
					returnedProperties[l] = propertiesAll[i];
					l++;
				}
			}
			*nbReturnedElements = iSizeArrayReturned;
		}

		delete [] propertiesAll;
		propertiesAll = NULL;
	}
	return returnedProperties;
}
/*--------------------------------------------------------------------------*/
BOOL ole_isMethod(IDispatch *pDisp, const wchar_t* methodToSearch)
{
    return ole_isFound(pDisp, methodToSearch, INVOKE_FUNC);
}
/*--------------------------------------------------------------------------*/
BOOL ole_isPropertyGet(IDispatch *pDisp, const wchar_t* propertyToSearch)
{
    return ole_isFound(pDisp, propertyToSearch, INVOKE_PROPERTYGET);
}
/*--------------------------------------------------------------------------*/
BOOL ole_isPropertyPut(IDispatch *pDisp, const wchar_t* propertyToSearch)
{
    return ole_isFound(pDisp, propertyToSearch, INVOKE_PROPERTYPUT);
}
/*--------------------------------------------------------------------------*/
