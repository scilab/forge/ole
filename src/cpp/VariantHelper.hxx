/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#ifndef __VARIANTHELPER_HXX__
#define __VARIANTHELPER_HXX__

#include <Ole2.h>

struct mixedMatrix 
{
	int rows;
	int cols;
	wchar_t **pWstrs;
	double *dValues;
};

#define VT_SAFEARRAYOFVARIANT 8204

enum SAFEARRAYTYPE 
{
    SAFEARRAY_TYPE_NOT_MANAGED = -3,
    SAFEARRAY_NOT_ARRAY = -2,
    SAFEARRAY_ERROR = -1,
    SAFEARRAY_MIXED = 0,
    SAFEARRAY_BOOL = 1,
    SAFEARRAY_DOUBLE = 2,
    SAFEARRAY_WIDESTRING = 3,
    SAFEARRAY_INT8 = 4,
    SAFEARRAY_UINT8 = 5,
    SAFEARRAY_INT16 = 6,
    SAFEARRAY_UINT16 = 7,
    SAFEARRAY_INT32 = 8,
    SAFEARRAY_UINT32 = 9,
    SAFEARRAY_EMPTY = 10,
    SAFEARRAY_VARIANT = 11,
    SAFEARRAY_NEED_CONVERSION_DOUBLE = 12,
    SAFEARRAY_NEED_CONVERSION_WIDESTRING = 13
};

bool isVariant(void *pVariant);
const wchar_t *getVariantTypeAsString(VARIANT *pVariant);
VARTYPE getVariantTypeFromString(const wchar_t *pStrVarType, int *iError);

VARIANT *emptyToVariant(int *iError);
VARIANT *doubleToVariant(double dValue, int *iError);
VARIANT *wstringToVariant(const wchar_t *pString, int *iError);
VARIANT *boolToVariant(BOOL bValue, int *iError);

VARIANT *int8ToVariant(char int8Value, int *iError);
VARIANT *unsignedint8ToVariant(unsigned char unsignedint8Value, int *iError);
VARIANT *int16ToVariant(short int16Value, int *iError);
VARIANT *unsignedint16ToVariant(unsigned short unsignedint16Value, int *iError);
VARIANT *int32ToVariant(int int32Value, int *iError);
VARIANT *unsignedint32ToVariant(unsigned int int32Value, int *iError);

VARIANT *matrixOfDoubleToVariant(double *dValues, int rows, int cols, int *iError);
SAFEARRAY *matrixOfDoubleToSafearray(double *dValues, int rows, int cols, int *iError);
SAFEARRAY *matrixOfDoubleToSafearrayVariant(double *dValues, int rows, int cols, int *iError);
VARIANT *matrixOfDoubleToVariantArray(double *dValues, int rows, int cols, int *iError);

VARIANT *matrixOfWideStringToVariant(const wchar_t **pStrings, int rows, int cols, int *iError);
VARIANT *matrixOfBoolToVariant(BOOL *bValues, int rows, int cols, int *iError);
VARIANT *mixedMatrixToVariant(mixedMatrix cell, int *iError);

VARIANT *matrixOfInt8ToVariant(char *int8Values, int rows, int cols, int *iError);
VARIANT *matrixOfUnsignedInt8ToVariant(unsigned char *unsignedint8Values, int rows, int cols, int *iError);
VARIANT *matrixOfInt16ToVariant(short *int16Values, int rows, int cols, int *iError);
VARIANT *matrixOfUnsignedInt16ToVariant(unsigned short *unsignedint16Values, int rows, int cols, int *iError);
VARIANT *matrixOfInt32ToVariant(int *int32Values, int rows, int cols, int *iError);
VARIANT *matrixOfUnsignedInt32ToVariant(unsigned int *unsignedint32Values, int rows, int cols, int *iError);

double variantToDouble(VARIANT *pvar, BOOL bConvertToNan, int *iError);
wchar_t *variantToWideString(VARIANT *pvar, int *iError);
BOOL variantToBool(VARIANT *pvar, int *iError);

char variantToInt8(VARIANT *pvar, BOOL bConvertToNan, int *iError);
unsigned char variantToUnsignedInt8(VARIANT *pvar, BOOL bConvertToNan, int *iError);
short variantToInt16(VARIANT *pvar, BOOL bConvertToNan, int *iError);
unsigned short variantToUnsignedInt16(VARIANT *pvar, BOOL bConvertToNan, int *iError);
int variantToInt32(VARIANT *pvar, BOOL bConvertToNan, int *iError);
unsigned int variantToUnsignedInt32(VARIANT *pvar, BOOL bConvertToNan, int *iError);

double *variantToMatrixOfDouble(VARIANT *pvar, int *rows, int *cols, int *iError);
double *safeArrayToMatrixOfDouble(SAFEARRAY *sfa, int *rows, int *cols, int *iError);
wchar_t **variantToMatrixOfWideString(VARIANT *pvar, int *rows, int *cols, int *iError);
BOOL *variantToMatrixOfBool(VARIANT *pvar, int *rows, int *cols, int *iError);
mixedMatrix * variantToMixedMatrix(VARIANT *pvar, int *iError);

char *variantToMatrixOfInt8(VARIANT *pvar, int *rows, int *cols, int *iError);
short *variantToMatrixOfInt16(VARIANT *pvar, int *rows, int *cols, int *iError);
int *variantToMatrixOfInt32(VARIANT *pvar, int *rows, int *cols, int *iError);

unsigned char *variantToMatrixOfUnsignedInt8(VARIANT *pvar, int *rows, int *cols, int *iError);
unsigned short *variantToMatrixOfUnsignedInt16(VARIANT *pvar, int *rows, int *cols, int *iError);
unsigned int *variantToMatrixOfUnsignedInt32(VARIANT *pvar, int *rows, int *cols, int *iError);

SAFEARRAYTYPE getSafeArrayType(VARIANT *pvar);

VARIANT *safeArrayOfVariantToSafeArrayOfWideString(VARIANT *pVariant, int *iError);

VARIANT * ArrayOfToSafeArray(VARIANT *pVariant, int *iError);

double extractDoublesFromArrayOfVariant(VARIANT *pVariant, int *rows, int *cols, int *iError);
double extractStringsFromArrayOfVariant(VARIANT *pVariant, int *rows, int *cols, int *iError);

HRESULT copyVariant(VARIANT *dstvar, VARIANT *srcvar);


int extractDataForMListFromArrayOfVariant(VARIANT *pVariant,
                                          int *rows, int *cols,
                                          wchar_t ***pwStrsValues,
                                          double **pdoublesValues);

#endif /* __VARIANTHELPER_HXX__ */
