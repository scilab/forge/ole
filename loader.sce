// ====================================================================
// Allan CORNET 
// Copyright DIGITEO 2008 - 2011
// ====================================================================
try
 v = getversion('scilab');
catch
 warning('Scilab 6.0 or more is required.');
 return;
end;
// ====================================================================
if ~(v(1)>=6) then
 warning('Scilab 6.0 or more is required.');
 return;
end
// ====================================================================
root_tlbx = get_absolute_file_path('loader.sce');
exec(root_tlbx + 'etc\' + 'automation.start');
// ====================================================================
clear root_tlbx;
clear v;
// ====================================================================
