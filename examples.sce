//==============================================================================
// call outlook
//==============================================================================
pOutlook = ole_actxserver("Outlook.Application")
pItem = ole_get(pOutlook, "CreateItem",0)
ole_set(pItem, "Subject","hello")
ole_set(pItem, "To","allan.cornet@scilab.org")
ole_set(pItem, "Body","test send email")
ole_invoke(pItem, "Display")
ole_invoke(pItem, "Send")
ole_delete(pItem)
ole_delete(pOutlook)
clear pOutlook pItem
//==============================================================================
// example: we call Word
//==============================================================================
pWord = ole_actxserver("Word.Application")
ole_set(pWord, "Visible", 1)
pDocs = ole_get(pWord, "Documents")
p= ole_invoke( pDocs, "Add")
pSelection = ole_invoke(pWord, "Selection")
ole_set(pSelection,"Text","Hello text in Word")
pActiveDocument = ole_get(pWord, "ActiveDocument")
ole_invoke(pActiveDocument, "SaveAs",TMPDIR + "/foo.doc")
ole_invoke(pWord, "Quit")
ole_delete()
clear pWord pDocs pSelection pActiveDocument
//==============================================================================
// read Word document
//==============================================================================
pWord = ole_actxserver("Word.Application")
ole_set(pWord, "Visible", 1)
pDocuments = ole_get(pWord, "Documents")
ole_invoke(pDocuments, "Open",TMPDIR + "/foo.doc");
pActiveDocument = ole_get(pWord, "ActiveDocument");
pContent  = ole_invoke(pActiveDocument,'Content');
pText = ole_invoke(pContent,'Text');
ole_invoke(pWord, "Quit");
ole_delete()
clear pWord pDocuments pActiveDocument pContent pText
//==============================================================================
// example: we call Internet Explorer
//==============================================================================
pIE = ole_actxserver("InternetExplorer.Application")
ole_invoke(pIE,"Navigate","http://www.scilab.org")
ole_set(pIE,"Visible",%t)
ole_invoke(pIE,"Quit")
ole_delete(pIE)
clear pIE
//==============================================================================
// example: we call Windows Player
//==============================================================================
pWMP =  ole_actxserver("Wmplayer.ocx")
ole_invoke(pWMP, "openPlayer", ole_getOlePath() + "demos/newsradio.mp3");
ole_delete(pWMP)
clear pWMP
//==============================================================================
// example: we call VBScript
//==============================================================================
pScript = ole_actxserver('MSScriptControl.ScriptControl')
ole_set(pScript, 'Language', 'VBScript')
ole_invoke(pScript,'Eval','MsgBox(""Hello world"")');
r = ole_invoke(pScript,'Eval','68 + 1')
ole_invoke(pScript,'Eval','MsgBox(""Nice MessageBox ?"",vbYesNo)')
ole_delete(pScript)
//==============================================================================
// example: we call a database via ADODB.Connection
//==============================================================================
// Require a database engine installed on your PC
// A Few example strings for common databases:
// MS SQL Server -  PROVIDER=SQLOLEDB; Data Source=myServerAddress; Initial Catalog=myDataBase; User Id=myUsername; Password=myPassword;
// MySQL  - driver=MySQL ODBC 3.51 Driver; Server=myServerAddress; Database=myDataBase; UID=username; PWD=password; 
// Access - PROVIDER=Microsoft.Jet.OLEDB.4.0; Data Source=C:\mydatabase.mdb;User Id=myUsername; Password=myPassword;
// Access 2007 - PROVIDER=Microsoft.ACE.OLEDB.12.0; Data Source=C:\mydatabase.accdb; Persist Security Info=False;
// Oracle - PROVIDER=OraOLEDB.Oracle; Data Source=MyOracleDB; User Id=myUsername; Password=myPassword;
pConn = ole_actxserver('ADODB.Connection')
commandInvoke = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' +  ole_getOlePath() + "demos/adotest.mdb" + ';User Id=admin;Password=;'
ole_set(pConn,'CursorLocation',3)
ole_invoke(pConn, 'Open', commandInvoke);
ole_set(pConn,'CommandTimeout',60)
sqlQuery = 'select LASTNAME from TestTable';
ole_invoke(pConn,'BeginTrans');
pData = ole_invoke(pConn, 'Execute', sqlQuery);
ole_invoke(pConn,'CommitTrans'); 
ole_get(pData,'recordcount')
data = ole_invoke(pData, 'getrows')
// another request
sqlQuery = 'select * from TestTable order by lastname, firstname';
ole_invoke(pConn,'BeginTrans');
pData = ole_invoke(pConn, 'Execute', sqlQuery);
ole_invoke(pConn,'CommitTrans'); 
ole_get(pData,'recordcount')
data = ole_invoke(pData, 'getrows')
//==============================================================================
// Example Scilab calls TextToSpeech
//==============================================================================
pTextToSpeech = ole_actxserver("Sapi.SpVoice")
ole_invoke(pTextToSpeech ,"Speak","Hello!!! Scilab uses TextToSpeech by COM Object")
//==============================================================================
// Call LabVIEW
pLabVIEW = ole_actxserver('Labview.Application')
viPath = 'Scilab SignalGen-Analysis.vi'
pVI = ole_invoke(pLabVIEW, "GetVIReference", viPath)
ole_set(pVI,'FPWinOpen', %t)
ole_invoke(pVI, 'Call')
//==============================================================================
// Read a xml file
//==============================================================================
xmlFileName = ole_getOlePath() + "demos/TheSimpsons.xml";
xmlDoc = ole_actxserver( 'Microsoft.XMLDOM');

// load xml file in memory
ole_set(xmlDoc, 'async', "false");
ole_invoke(xmlDoc, 'load', xmlFileName);

// plain text
ole_get(xmlDoc,'text')

// Get Name of Characters
oElement = ole_get(xmlDoc,'documentElement');
x = ole_invoke(oElement, 'getElementsByTagName' ,"name");
Lx = ole_get(x,'length');
for i = 0:Lx-1
  pItem = ole_get(x, 'item', i);
  disp(ole_get(pItem,'text'));
end

// Get Firstname of Characters
oElement = ole_get(xmlDoc,'documentElement');
x = ole_invoke(oElement, 'getElementsByTagName' ,"firstname");
Lx = ole_get(x,'length');
for i = 0:Lx-1
  pItem = ole_get(x, 'item', i);
  disp(ole_get(pItem,'text'));
end
//==============================================================================
// write a xml file
xmlDoc = ole_actxserver("Microsoft.XMLDOM")

// create root element
oRoot = ole_invoke(xmlDoc,"createElement","Root");
ole_invoke(xmlDoc,"appendChild",oRoot);

// add element
oElement = ole_invoke(xmlDoc, "selectSingleNode", "Root");
// create a child
oElement = ole_invoke(xmlDoc, "createElement" , "element"); 

pdocumentElement = ole_get(xmlDoc, "documentElement");
// add child to root element
ole_invoke(pdocumentElement, "appendChild", oElement);

oName = ole_invoke(xmlDoc,"createElement","name");
ole_set(oName,"Text", "CORNET");
ole_invoke(oElement, "appendChild", oName);

oFirstname = ole_invoke(xmlDoc,"createElement","firstname");
ole_set(oFirstname,"Text", "Allan");
ole_invoke(oElement, "appendChild", oFirstname);

// Save file
mdelete(TMPDIR + "/write.xml");
ole_invoke(xmlDoc, "Save", TMPDIR + "/write.xml");
disp(mgetl(TMPDIR + "/write.xml"))
//==============================================================================
