// ====================================================================
// Antoine ELIAS
// Copyright Scilab Enterprises 2013
// ====================================================================

mode(-1);
lines(0);

function main_cleaner()

  TOOLBOX_NAME  = "automation";
  TOOLBOX_TITLE = "automation";
  toolbox_dir   = get_absolute_file_path("cleaner.sce");
// =============================================================================
// Check Scilab's version
// =============================================================================
try
  v = getversion("scilab");
catch
  error(gettext("Scilab 5.4 or more is required."));
end

if v(2) < 4 then
  error(gettext('Scilab 5.4 or more is required.'));
end
clear v;
// =============================================================================
// Action
// =============================================================================
if getos() == 'Windows' then
    setenv('SCIDir', SCI + filesep());

    compiler = findmsvccompiler();
    if strstr(compiler, "express") <> "" then
        compilerbin = "VCExpress.exe ";
    else
        compilerbin = "devenv.exe ";
    end
        
    if win64() then
        if (getenv('DEBUG_SCILAB_DYNAMIC_LINK','NO') == 'YES') then
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Clean ""Debug|x64""';
        else
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Clean ""Release|x64""';
        end
    else
        if (getenv('DEBUG_SCILAB_DYNAMIC_LINK','NO') == 'YES') then
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Clean ""Debug|Win32""';
        else
            cmdline = compilerbin + toolbox_dir + 'automation.sln /Clean ""Release|Win32""';
        end
    end

    [output, bOK, exitcode] = dos(cmdline);
    if (ilib_verbose() > 1 | ~bOK) then
        disp(output);
    end

    clear output;
    clear bOK;
    clear exitcode;
    clear cmdline;

    clear findmsvccompiler;

    if isfile(toolbox_dir + '/macros/cleanmacros.sce') then
        exec(toolbox_dir + '/macros/cleanmacros.sce');
    end
else
    warning("Only for Windows Environment.");
end

endfunction
// =============================================================================
main_cleaner();
clear main_cleaner; // remove main_builder on stack
// =============================================================================


