/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2010 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "VariantHelper.hxx"
#include "VariantsManager.hxx"
#include "Scierror.h"
#include "localization.h"
/*--------------------------------------------------------------------------*/
static BOOL deleteVariant(VARIANT *pVariant);
/*--------------------------------------------------------------------------*/
int sci_ole_delete(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	void* pvPtr = NULL;
	VARIANT *pVariant = NULL;
    BOOL bRes = FALSE;

	CheckRhs(0, 1);
	CheckLhs(0, 1);

    if (Rhs == 0)
    {
        pVariant = getLastVariantInManager();
        bRes = TRUE;
        while (pVariant)
        {
            deleteVariant(pVariant);
			pVariant = getLastVariantInManager();
        } 
    }
    else
    {

        sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isPointerType(pvApiCtx, piAddrOne))
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: pointer expected.\n"),fname, 1);
            return 0;
        }

        sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isVariant(pvPtr))
        {
            Scierror(999, "%s: Not a valid pointer on a Variant.\n", fname);
        }

        pVariant = (VARIANT *)pvPtr;
        bRes = deleteVariant(pVariant);
    }
    createScalarBoolean(pvApiCtx, Rhs + 1, bRes);

	LhsVar(1) = Rhs + 1;	
	PutLhsVar();
	return 0;
}
/*--------------------------------------------------------------------------*/
static BOOL deleteVariant(VARIANT *pVariant)
{
	__try
	{
		removeVariantInManager(pVariant);
		if (pVariant->vt == VT_DISPATCH)
		{
			pVariant->pdispVal->Release();
		}
		VariantClear(pVariant);
		if (!SUCCEEDED(VariantClear(pVariant)))
		{
			return FALSE;
		}
	}

	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		return FALSE;
	}
	
	return TRUE;
}
/*--------------------------------------------------------------------------*/
