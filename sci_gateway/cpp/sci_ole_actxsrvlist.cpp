/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_automation.h"
#include "GetInterfacesFromRegistry.hxx"
/*--------------------------------------------------------------------------*/
int sci_ole_actxsrvlist(char *fname, void* pvApiCtx)
{
    int iError = 0;
    COM_INTERFACE_INFO *pInterfaces = NULL;
    Rhs = max(Rhs, 0);
    CheckRhs(0, 0);
    CheckLhs(1, 3);

    pInterfaces = GetInterfacesFromRegistry(&iError);
    if (iError == 0)
    {
        if (pInterfaces->nbElements == 0)
        {
            if (Lhs >= 1)
            {
                createEmptyMatrix(pvApiCtx, Rhs + 1);
                LhsVar(1) = Rhs + 1;	
            }

            if (Lhs >= 2)
            {
                createEmptyMatrix(pvApiCtx, Rhs + 2);
                LhsVar(2) = Rhs + 2;	
            }

            if (Lhs == 3)
            {
                createEmptyMatrix(pvApiCtx, Rhs + 3);
                LhsVar(3) = Rhs + 3;	
            }
        }
        else
        {
            SciErr sciErr;
            if (Lhs >= 1)
            {
                sciErr = createMatrixOfWideString(pvApiCtx, Rhs + 1, pInterfaces->nbElements, 1, pInterfaces->wName);
                if(sciErr.iErr)
                {
                    deleteInterfacesInfo(pInterfaces);
                    printError(&sciErr, 0);
                    return 0;
                }
                LhsVar(1) = Rhs + 1;
            }

            if (Lhs >= 2)
            {
                sciErr = createMatrixOfWideString(pvApiCtx, Rhs + 2, pInterfaces->nbElements, 1, pInterfaces->wProgID);
                if(sciErr.iErr)
                {
                    deleteInterfacesInfo(pInterfaces);
                    printError(&sciErr, 0);
                    return 0;
                }
                LhsVar(2) = Rhs + 2;
            }

            if (Lhs == 3)
            {
                sciErr = createMatrixOfWideString(pvApiCtx, Rhs + 3, pInterfaces->nbElements, 1, pInterfaces->wFile);
                if(sciErr.iErr)
                {
                    deleteInterfacesInfo(pInterfaces);
                    printError(&sciErr, 0);
                    return 0;
                }
                LhsVar(3) = Rhs + 3;
            }
        }

        deleteInterfacesInfo(pInterfaces);

        PutLhsVar();
    }
    else
    {
        Scierror(999,_("%s: Can not get information.\n"), fname);
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
