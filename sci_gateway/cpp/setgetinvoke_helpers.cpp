/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include "setgetinvoke_helpers.hxx"
#include "api_scilab.h"
//#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "VariantHelper.hxx"
#include "createMListVariant.h"
/*--------------------------------------------------------------------------*/
static int VT_EMPTYonStack(void *pvApiCtx, int stackPosition)
{
    return createEmptyMatrix(pvApiCtx, stackPosition);
}
/*--------------------------------------------------------------------------*/
static int VT_RXonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    double dValue = variantToDouble(pVariant, TRUE, &iError);
    if (iError == 0)
    {
        iError = createScalarDouble(pvApiCtx, stackPosition, dValue);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_BSTRonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    wchar_t *pwStr = variantToWideString(pVariant, &iError);
    if (iError == 0)
    {
        if (pwStr)
        {
            iError = createSingleWideString(pvApiCtx, stackPosition, pwStr);
            delete pwStr;
            pwStr = NULL;
        }
        else
        {
            iError = 1;
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_BOOLonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    BOOL bValue = variantToBool(pVariant, &iError);

    if (iError == 0)
    {
        iError = createScalarBoolean(pvApiCtx, stackPosition, bValue);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_UI1onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    unsigned char unsignedint8Value = variantToUnsignedInt8(pVariant, TRUE, &iError);

    if (iError == 0)
    {
        iError = createScalarUnsignedInteger8(pvApiCtx, stackPosition, unsignedint8Value);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_UI2onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    unsigned short unsignedint16Value = variantToUnsignedInt16(pVariant, TRUE, &iError);

    if (iError == 0)
    {
        iError = createScalarUnsignedInteger16(pvApiCtx, stackPosition, unsignedint16Value);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_I1onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    char int8Value = variantToInt8(pVariant, TRUE, &iError);

    if (iError == 0)
    {
        iError = createScalarInteger8(pvApiCtx, stackPosition, int8Value);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_I2onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    short int16Value = variantToInt16(pVariant, TRUE, &iError);

    if (iError == 0)
    {
        iError = createScalarInteger16(pvApiCtx, stackPosition, int16Value);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_INTonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    int iValue = variantToInt32(pVariant, TRUE, &iError);

    if (iError == 0)
    {
        iError = createScalarInteger32(pvApiCtx, stackPosition, iValue);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_MIXEDonStack(void *pvApiCtx, int /*stackPosition*/, VARIANT* /*pVariant*/)
{
    return 1;
}
/*--------------------------------------------------------------------------*/
static int SA_BOOLonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;
    BOOL *pboolValues = variantToMatrixOfBool(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfBoolean(pvApiCtx, stackPosition, m, n, pboolValues);

        delete [] pboolValues;
        pboolValues = NULL;

        iError = sciErr.iErr;
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_DOUBLEonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;
    double *pdoubleValues = variantToMatrixOfDouble(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfDouble(pvApiCtx, stackPosition, m, n, pdoubleValues);

        delete [] pdoubleValues;
        pdoubleValues = NULL;

        iError = sciErr.iErr;
        if(iError)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_WIDESTRINGonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;

    wchar_t **pwStrValues = variantToMatrixOfWideString(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfWideString(pvApiCtx, stackPosition, m, n, pwStrValues);

        for (int i = 0; i < m * n; i++)
        {
            delete pwStrValues[i];
            pwStrValues[i] = NULL;
        }

        delete [] pwStrValues;
        pwStrValues = NULL;

        iError = sciErr.iErr;
        if(iError)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_INT32onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;

    int *pint32Values = variantToMatrixOfInt32(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfInteger32(pvApiCtx, stackPosition, m, n, pint32Values);

        delete [] pint32Values;
        pint32Values = NULL;

        iError = sciErr.iErr;
        if(iError)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_INT16onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;

    short *pint16Values = variantToMatrixOfInt16(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfInteger16(pvApiCtx, stackPosition, m, n, pint16Values);

        delete [] pint16Values;
        pint16Values = NULL;

        iError = sciErr.iErr;
        if(iError)
        {
            printError(&sciErr, 0);
            return 0;
        }
	}
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_INT8onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;

    char *pint8Values = variantToMatrixOfInt8(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfInteger8(pvApiCtx, stackPosition, m, n, pint8Values);

        delete [] pint8Values;
        pint8Values = NULL;

        iError = sciErr.iErr;
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_UINT8onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;

    unsigned char *puint8Values = variantToMatrixOfUnsignedInt8(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfUnsignedInteger8(pvApiCtx, stackPosition, m, n, puint8Values);

        delete [] puint8Values;
        puint8Values = NULL;

        iError = sciErr.iErr;
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_UINT16onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;
    unsigned short *puint16Values = variantToMatrixOfUnsignedInt16(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfUnsignedInteger16(pvApiCtx, stackPosition, m, n, puint16Values);

        delete [] puint16Values;
        puint16Values = NULL;

        iError = sciErr.iErr;
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int SA_UINT32onStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    SciErr sciErr;
    int iError = 1;
    int m = 0;
    int n = 0;

    unsigned int *puint32Values = variantToMatrixOfUnsignedInt32(pVariant, &m, &n, &iError);
    if (iError == 0)
    {
        sciErr = createMatrixOfUnsignedInteger32(pvApiCtx, stackPosition, m, n, puint32Values);

        delete [] puint32Values;
        puint32Values = NULL;

        iError = sciErr.iErr;
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
        }
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_SAFEARRAYonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;

    switch(getSafeArrayType(pVariant))
    {

    case SAFEARRAY_MIXED:
        {
            iError = SA_MIXEDonStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_BOOL:
        {
            iError = SA_BOOLonStack(pvApiCtx, stackPosition, pVariant);
        }
        break;

    case SAFEARRAY_NEED_CONVERSION_DOUBLE:
    case SAFEARRAY_DOUBLE:
        {
            iError = SA_DOUBLEonStack(pvApiCtx, stackPosition, pVariant);
        }
        break;

    case SAFEARRAY_NEED_CONVERSION_WIDESTRING:
    case SAFEARRAY_WIDESTRING:
        {
            iError = SA_WIDESTRINGonStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_INT32:
        {
            iError = SA_INT32onStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_INT16:
        {
            iError = SA_INT16onStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_INT8:
        {
            iError = SA_INT8onStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_UINT8:
        {
            iError = SA_UINT8onStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_UINT16:
        {
            iError = SA_UINT16onStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_UINT32:
        {
            iError = SA_UINT32onStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_EMPTY:
        {
            iError = VT_EMPTYonStack(pvApiCtx, stackPosition);
        }
        break;

    case SAFEARRAY_VARIANT:
        {
            iError = SA_WIDESTRINGonStack(pvApiCtx, stackPosition, pVariant);
        }
        break;
    case SAFEARRAY_ERROR:
    case SAFEARRAY_TYPE_NOT_MANAGED:
    case SAFEARRAY_NOT_ARRAY:
    default:
        {
            iError = 1;
        }
        break;
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
static int VT_UINTonStack(void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    unsigned int unsignedint32Value = variantToUnsignedInt32(pVariant, TRUE, &iError);

    if (iError == 0)
    {
        iError = createScalarUnsignedInteger32(pvApiCtx, stackPosition, unsignedint32Value);
    }
    return iError;
}
/*--------------------------------------------------------------------------*/
int sgi_checkTypeOptionalInputArguments(char *fname, void *pvApiCtx)
{
    for (int j = firstArgPosToCheck; j <= Rhs; j++)
    {
        SciErr sciErr;
        int *piAddr = NULL;
        int iType = 0;

        sciErr = getVarAddressFromPosition(pvApiCtx, j, &piAddr);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        sciErr = getVarType(pvApiCtx,  piAddr, &iType);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        switch(iType)
        {
        case sci_boolean:
        case sci_ints:
        case sci_matrix:
        case sci_strings:
        case sci_pointer:
            {
                // supported types
            }
            break;
        default:
            {
                Scierror(999, _("%s: Wrong type for input argument #%d.\n"),fname, j);
                return 0;
            }
            break;
        }       
    }
    return 1;
}
/*--------------------------------------------------------------------------*/
int sgi_checkSizeOptionalInputArguments(char *fname, void *pvApiCtx)
{
    for (int j = firstArgPosToCheck; j <= Rhs; j++)
    {
        int *piAddr = NULL;
        SciErr sciErr = getVarAddressFromPosition(pvApiCtx, j, &piAddr);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (!isPointerType(pvApiCtx,  piAddr))
        {
			if (!isScalar(pvApiCtx,  piAddr) && !isVector(pvApiCtx,  piAddr))
            {
                Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, j);
                return 0;
            }
        }
    }
    return 1;
}
/*--------------------------------------------------------------------------*/
VARIANT * sgi_getVariantsOptionalInputArguments(char *fname, void *pvApiCtx, int *ierror, int *nbArgumentsReturned)
{
    VARIANT *pVariants = NULL;
    *ierror = 1;

    *nbArgumentsReturned = 0;
	int sizeTabVariant = Rhs - (firstArgPosToCheck - 1);
    pVariants = new VARIANT[sizeTabVariant];

    for (int j = firstArgPosToCheck; j <= Rhs; j++)
    {
        SciErr sciErr;
        int *piAddr = NULL;
        int iType = 0;

        sciErr = getVarAddressFromPosition(pvApiCtx, j, &piAddr);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            delete [] pVariants;
            pVariants = NULL;
            return NULL;
        }

        sciErr = getVarType(pvApiCtx,  piAddr, &iType);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            delete [] pVariants;
            pVariants = NULL;
            return NULL;
        }

        int ierr = 0;
        VARIANT *pVarTmp = NULL;

        switch(iType)
        {
        case sci_boolean:
            {
                BOOL bValue = FALSE;
                getScalarBoolean(pvApiCtx, piAddr, &bValue);
                pVarTmp = boolToVariant(bValue, &ierr);
            }
            break;
        case sci_ints:
            {
                int iPrecision = 0;
                sciErr = getMatrixOfIntegerPrecision(pvApiCtx, piAddr, &iPrecision);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    delete [] pVariants;
                    pVariants = NULL;
                    return NULL;
                }

                switch (iPrecision)
                {
                case SCI_INT8:
                    {
                        char int8Value = 0;
                        getScalarInteger8(pvApiCtx, piAddr, &int8Value);
                        pVarTmp = int8ToVariant(int8Value, &ierr);
                    }
                    break;
                case SCI_INT16:
                    {
                        short int16Value = 0;
                        getScalarInteger16(pvApiCtx, piAddr, &int16Value);
                        pVarTmp = int16ToVariant(int16Value, &ierr);
                    }
                    break;
                case SCI_INT32:
                    {
                        int int32Value = 0;
                        getScalarInteger32(pvApiCtx, piAddr, &int32Value);
                        pVarTmp = int32ToVariant(int32Value, &ierr);
                    }
                    break;
                case SCI_UINT8:
                    {
                        unsigned char unsignedint8Value = 0;
                        getScalarUnsignedInteger8(pvApiCtx, piAddr, &unsignedint8Value);
                        pVarTmp = int16ToVariant(unsignedint8Value, &ierr);
                    }
                    break;
                case SCI_UINT16:
                    {
                        unsigned short unsignedint16Value = 0;
                        getScalarUnsignedInteger16(pvApiCtx, piAddr, &unsignedint16Value);
                        pVarTmp = int16ToVariant(unsignedint16Value, &ierr);
                    }
                    break;
                case SCI_UINT32:
                    {
                        unsigned int unsignedint32Value = 0;
                        getScalarUnsignedInteger32(pvApiCtx, piAddr, &unsignedint32Value);
                        pVarTmp = unsignedint32ToVariant(unsignedint32Value, &ierr);

                    }
                    break;
                }
            }
            break;
        case sci_matrix:
            {
				int nRows;
				int nCols;
				double* pdblReal = NULL;
				double* pdblImg = NULL;
				
				sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &nRows, &nCols, &pdblReal);
				if (sciErr.iErr)
				{
					printError(&sciErr, 0);
					return NULL;
				}
				else
				{
					if(nRows*nCols==1)
						pVarTmp = doubleToVariant(*pdblReal,  &ierr);
					else
						pVarTmp = matrixOfDoubleToVariantArray(pdblReal, nRows, nCols, &ierr);
					if(ierr)
						return NULL;
				}
			}
            break;
        case sci_strings:
            {
                wchar_t *pwStr = NULL;
                getAllocatedSingleWideString(pvApiCtx, piAddr, &pwStr);
                pVarTmp = wstringToVariant(pwStr, &ierr);
                freeAllocatedSingleWideString(pwStr);
                pwStr = NULL;
            }
            break;
        case sci_pointer:
            {
                void* pvPtr = NULL;
                sciErr = getPointer(pvApiCtx, piAddr, &pvPtr);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    ierr = 1;
                }

                if (!isVariant(pvPtr))
                {
                    Scierror(999,_("%s: Not a valid pointer on a Variant.\n"),fname);
                    ierr = 1;
                }
				else
				{
					pVarTmp = (VARIANT *)pvPtr;
				}
				
            }
            break;
        }

        if (ierr == 1)
        {
            delete pVarTmp;
            pVarTmp = NULL;

            delete [] pVariants;
            pVariants = NULL;

            return NULL;
        }


		//HRESULT hRes = VariantCopy(&(pVariants[j - firstArgPosToCheck]), pVarTmp);
        //memcpy((void*)&pVariants[sizeTabVariant -(j - firstArgPosToCheck) - 1],pVarTmp, sizeof(VARIANT));
		copyVariant(&pVariants[sizeTabVariant -(j - firstArgPosToCheck) - 1], pVarTmp);
		
		if (iType != sci_pointer)
		{
			delete pVarTmp;
			pVarTmp = NULL;
		}
    }


    *nbArgumentsReturned = Rhs - (firstArgPosToCheck - 1);
    *ierror = 0;
    return pVariants;
}
/*--------------------------------------------------------------------------*/
char *getInvokeErrorMessage(INVOKE_ERROR InvokeError)
{
    char *msg_error = NULL;

    switch(InvokeError)
    {
    case INVOKE_NULL_IDISPATCH:
        msg_error = _strdup("IDispatch NULL");
        break;
    case INVOKE_GETIDSOFNAMES:
        msg_error = _strdup("Invalid GetIdsOfNames");
        break;
    case INVOKE_BADPARAMCOUNT:
        msg_error = _strdup("Bad Parameters Count");
        break;
    case INVOKE_BADVARTYPE:
        msg_error = _strdup("Bad Variable Type");
        break;
    case INVOKE_EXCEPTION:
        msg_error = _strdup("Exception");
        break;
    case INVOKE_MEMBERNOTFOUND: 
        msg_error = _strdup("Member Not Found");
        break;
    case INVOKE_NONAMEDARGS:
        msg_error = _strdup("No Named Arguments");
        break;
    case INVOKE_OVERFLOW:
        msg_error = _strdup("Overflow");
        break;
    case INVOKE_PARAMNOTFOUND: 
        msg_error = _strdup("Parameter Not Found");
        break;
    case INVOKE_TYPEMISMATCH:
        msg_error = _strdup("Type Mismatch");
        break;
    case INVOKE_UNKNOWNINTERFACE:
        msg_error = _strdup("Unknown Interface");
        break;
    case INVOKE_UNKNOWNLCID:
        msg_error = _strdup("Unknown Lcid");
        break;
    case INVOKE_PARAMNOTOPTIONAL:
        msg_error = _strdup("Parameter Not Optional");
        break;
    default:
        msg_error = _strdup("Unknown");
        break;
    }
    return msg_error;
}
/*--------------------------------------------------------------------------*/
int variantToScilabTypeOnStack(const char *fname, void *pvApiCtx, int stackPosition, VARIANT *pVariant)
{
    int iError = 0;
    switch (pVariant->vt)
    {
    case VT_SAFEARRAY:
        iError = VT_SAFEARRAYonStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_EMPTY:
        iError = VT_EMPTYonStack(pvApiCtx, stackPosition);
        break;

    case VT_R4:
    case VT_R8:  
        iError = VT_RXonStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_BSTR:
        iError = VT_BSTRonStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_BOOL:
        iError = VT_BOOLonStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_UI1:
        iError = VT_UI1onStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_UI2:
        iError = VT_UI2onStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_I1:
        iError = VT_I1onStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_I2:
        iError = VT_I2onStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_UI4:
    case VT_UI8:     
    case VT_I4:
    case VT_I8:
    case VT_INT:    
        iError = VT_INTonStack(pvApiCtx, stackPosition, pVariant);
        break;

    case VT_UINT: 
        iError = VT_UINTonStack(pvApiCtx, stackPosition, pVariant);
        break;

    default:
        if (pVariant->vt & VT_ARRAY)
        {
            int subType = pVariant->vt & VT_TYPEMASK;
            if (subType == VT_VARIANT)
            {
                double *pDoubleValues = NULL;
                wchar_t **pWStrValues = NULL;
                int m = 0;
                int n = 0;

                iError = extractDataForMListFromArrayOfVariant(pVariant,
                                                                &m, &n,
                                                                &pWStrValues,&pDoubleValues);
                if (iError == 0)
                {
                    SciErr sciErr = createMListVariant(pvApiCtx, 
                                                       stackPosition,
                                                       m, n, 
                                                       (const wchar_t **)pWStrValues, (const double *)pDoubleValues);
                    iError = sciErr.iErr;
                }
            }
        }
        else
        {
            iError = 1;
        }
        break;
    }


    if (iError)
    {
        Scierror(999, "%s: can not converted this VARIANT to Scilab.\n", fname);
    }
    else
    {
        LhsVar(1) = Rhs + 1;	
        PutLhsVar();
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
