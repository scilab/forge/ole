/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "Scierror.h"
#include "localization.h"
#include "VariantHelper.hxx"
#include "VariantsManager.hxx"
/*--------------------------------------------------------------------------*/
int sci_ole_variantChangeType(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	int* piAddrTwo = NULL;

    CheckRhs(2, 2);
    CheckLhs(0, 1);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddrTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (isScalar(pvApiCtx, piAddrTwo) == 0)
	{
		Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 2);
		return 0;
	}

	if (isPointerType(pvApiCtx, piAddrOne))
	{
		void* pvPtr = NULL;
		sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if (isVariant(pvPtr))
		{
			wchar_t *newVarTypeName = NULL;
			VARIANT *pVariant = (VARIANT *)pvPtr;

			if (getAllocatedSingleWideString(pvApiCtx, piAddrTwo, &newVarTypeName) != 0)
			{
				Scierror(999,_("%s: No more memory.\n"), fname);
				return 0;
			}

			int iError = 0;
			VARTYPE newVarType = getVariantTypeFromString(newVarTypeName, &iError);
			freeAllocatedSingleWideString(newVarTypeName);
			newVarTypeName = NULL;

			if (iError)
			{
				Scierror(999,_("%s: Wrong value for input argument #%d.\n"),fname, 2);
				return 0;
			}

			VARIANT *variantConverted = new VARIANT;
			VariantInit(variantConverted);
			if (SUCCEEDED(VariantChangeType(variantConverted, pVariant, VARIANT_NOUSEROVERRIDE, newVarType)))
			{
				sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)variantConverted);
				if(sciErr.iErr)
				{
					delete variantConverted;
					variantConverted = NULL;
					printError(&sciErr, 0);
					return 0;
				}

				addVariantInManager(variantConverted);
				
				LhsVar(1) = Rhs + 1;	
				PutLhsVar();
			}
			else
			{
				delete variantConverted;
				variantConverted = NULL;
				Scierror(999,_("%s: cannot convert to this VARIANT type.\n"),fname);
				return 0;
			}
		}
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
