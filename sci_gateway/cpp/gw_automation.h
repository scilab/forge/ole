/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2010 */
/*--------------------------------------------------------------------------*/
#include "machine.h"
#include "dynlib_automation.h"

#ifdef __cplusplus
extern "C"
{
#endif

	//IMPORT_EXPORT_AUTOMATION_DLL int C2F(gw_automation)(void* pvApiCtx);
	IMPORT_EXPORT_AUTOMATION_DLL int gw_automation(wchar_t *pwstFuncName);
	int sci_ole_actxserver(char *fname, void* pvApiCtx);
    int sci_ole_actxGetRunningSrv(char *fname, void* pvApiCtx);
	int sci_ole_invoke(char *fname, void* pvApiCtx);
    int sci_ole_set(char *fname, void* pvApiCtx);
    int sci_ole_get(char *fname, void* pvApiCtx);
	int sci_ole_variantToScilab(char *fname, void* pvApiCtx);
	int sci_ole_scilabToVariant(char *fname, void* pvApiCtx);
	int sci_ole_methods(char *fname, void* pvApiCtx);
	int sci_ole_properties(char *fname, void* pvApiCtx);
	int sci_ole_getVariantType(char *fname, void* pvApiCtx);
	int sci_ole_delete(char *fname, void* pvApiCtx);
	int sci_ole_iscom(char *fname, void* pvApiCtx);
    int sci_ole_isprop(char *fname, void* pvApiCtx);
    int sci_ole_ismethod(char *fname, void* pvApiCtx);
	int sci_ole_variantChangeType(char *fname, void* pvApiCtx);
    int sci_ole_actxsrvlist(char *fname, void* pvApiCtx);
    int sci_ole_mode(char *fname, void* pvApiCtx);
    int sci_ole_classInfo(char *fname, void* pvApiCtx);
    
#ifdef __cplusplus
};
#endif
/*--------------------------------------------------------------------------*/
