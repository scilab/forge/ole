/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "AutomationHelper.hxx"
#include "Scierror.h"
#include "localization.h"
#include "VariantsManager.hxx"
#include "VariantHelper.hxx"
/*--------------------------------------------------------------------------*/
static int sci_ole_properties_two_rhs(char *fname, void* pvApiCtx);
static int sci_ole_properties_one_rhs(char *fname, void* pvApiCtx);
/*--------------------------------------------------------------------------*/
int sci_ole_properties(char *fname, void* pvApiCtx)
{
	CheckRhs(1, 2);
	CheckLhs(0, 1);

	if (Rhs == 1)
	{
		return sci_ole_properties_one_rhs(fname, pvApiCtx);
	}
	else
	{
		return sci_ole_properties_two_rhs(fname, pvApiCtx);
	}
}
/*--------------------------------------------------------------------------*/
static int sci_ole_properties_two_rhs(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	int* piAddrTwo = NULL;

	void* pvPtr = NULL;
	VARIANT *pVariantDisp = NULL;
	wchar_t *ArgTwo = NULL;
	wchar_t **pstrsResult = NULL;
	int iSizeResult = 0;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddrTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (!isPointerType(pvApiCtx, piAddrOne))
	{
		Scierror(999,_("%s: Wrong type for input argument #%d.\n"),fname, 1);
		return 0;
	}

	if (isScalar(pvApiCtx, piAddrTwo) == 0)
	{
		Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 2);
		return 0;
	}

	sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (!isVariant(pvPtr))
	{
		Scierror(999, "Not a valid pointer on a Variant.\n");
		return 0;
	}

	pVariantDisp = (VARIANT *)pvPtr;
	if (pVariantDisp->vt != VT_DISPATCH)
	{
		Scierror(999, "%s: Invalid VARIANT type.\n", fname);
		return 0;
	}

	if (getAllocatedSingleWideString(pvApiCtx, piAddrTwo, &ArgTwo) != 0)
	{
		Scierror(999,_("%s: No more memory.\n"), fname);
		return 0;
	}

	bool bOK = false;
	if (wcscmp(ArgTwo, L"set") == 0)
	{
		pstrsResult = getPropertyPutsName(pVariantDisp->pdispVal, &iSizeResult);
		bOK = true;
	}
	else if (wcscmp(ArgTwo, L"get") == 0)
	{
		pstrsResult = getPropertyGetsName(pVariantDisp->pdispVal, &iSizeResult);
		bOK = true;
	}
	else
	{
		Scierror(999,_("%s: Wrong value for input argument #%d.\n"),fname, 2);
		bOK = false;
	}

	freeAllocatedSingleWideString(ArgTwo);
	ArgTwo = NULL;

	if (bOK)
	{
		if (iSizeResult > 0)
		{
			sciErr = createMatrixOfWideString(pvApiCtx, Rhs + 1, iSizeResult, 1, pstrsResult);

			for (int i = 0; i < iSizeResult; i++)
			{
				if (pstrsResult[i])
				{
					delete pstrsResult[i];
					pstrsResult[i] = NULL;
				}
			}
			delete [] pstrsResult;
			pstrsResult = NULL;

			if(sciErr.iErr)
			{
				printError(&sciErr, 0);
				return 0;
			}
		}
		else
		{
			createEmptyMatrix(pvApiCtx, Rhs + 1);
		}

		LhsVar(1) = Rhs + 1;	
		PutLhsVar();
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
static int sci_ole_properties_one_rhs(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);

	if (isPointerType(pvApiCtx, piAddrOne))
	{
		void* pvPtr = NULL;
		sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if (isVariant(pvPtr))
		{
			VARIANT *pVariantDisp = (VARIANT *)pvPtr;

			if (pVariantDisp->vt != VT_DISPATCH)
			{
				Scierror(999, "%s: Invalid VARIANT type.\n", fname);
				return 0;
			}
			else
			{
				int iSizePropertiesNames = 0;
				wchar_t **propertiesNames = getPropertiesName(pVariantDisp->pdispVal, &iSizePropertiesNames);
				if (propertiesNames && (iSizePropertiesNames > 0))
				{
					sciErr = createMatrixOfWideString(pvApiCtx, Rhs + 1, iSizePropertiesNames, 1, propertiesNames);

					for (int i = 0; i < iSizePropertiesNames; i++)
					{
						if (propertiesNames[i])
						{
							delete propertiesNames[i];
							propertiesNames[i] = NULL;
						}
					}
					delete [] propertiesNames;
					propertiesNames = NULL;

					if(sciErr.iErr)
					{
						printError(&sciErr, 0);
						return 0;
					}
				}
				else
				{
					createEmptyMatrix(pvApiCtx, Rhs + 1);
				}

				LhsVar(1) = Rhs + 1;	
				PutLhsVar();
			}
		}
		else
		{
			Scierror(999, "Not a valid pointer on a Variant.\n");
			return 0;
		}
	}
	else
	{
		Scierror(999,_("%s: Wrong type for input argument #%d.\n"),fname, 1);
		return 0;
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
