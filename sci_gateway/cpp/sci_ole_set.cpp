/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "gw_automation.h"
#include "AutomationHelper.hxx"
#include "VariantHelper.hxx"
#include "VariantsManager.hxx"
#include "setgetinvoke_helpers.hxx"
/*--------------------------------------------------------------------------*/
int sci_ole_set(char *fname, void* pvApiCtx)
{
    #define NBARGSIN 10
    INVOKE_ERROR InvokeError = INVOKE_NO_ERROR;
    VARIANT *variantArgs = NULL;
    int nbArguments = 0;
    int iErr = 0;
    SciErr sciErr;
    int* piAddrOne = NULL;
    int* piAddrTwo = NULL;

    VARIANT *pVariantDisp = NULL;
    wchar_t *propertyName = NULL;
    void* pvPtr = NULL;

    CheckRhs(2, NBARGSINMAX + 2);
    CheckLhs(0, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddrTwo);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (!isPointerType(pvApiCtx, piAddrOne))
    {
        Scierror(999,_("%s: Wrong type for input argument #%d.\n"),fname, 1);
        return 0;
    }

    if (isScalar(pvApiCtx, piAddrTwo) == 0)
    {
        Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 2);
        return 0;
    }

    sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (!isVariant(pvPtr))
    {
        Scierror(999, "Not a valid pointer on a Variant.\n");
        return 0;
    }

    pVariantDisp = (VARIANT *)pvPtr;
    if (pVariantDisp->vt != VT_DISPATCH)
    {
        Scierror(999, "%s: Invalid VARIANT type.\n", fname);
        return 0;
    }

    if (getAllocatedSingleWideString(pvApiCtx, piAddrTwo, &propertyName) != 0)
    {
        Scierror(999,_("%s: No more memory.\n"), fname);
        return 0;
    }

    if (!ole_isPropertyPut(pVariantDisp->pdispVal, propertyName) &&
         ole_isPropertyGet(pVariantDisp->pdispVal, propertyName))
    {
        freeAllocatedSingleWideString(propertyName);
        propertyName = NULL;

        Scierror(999,_("%s: This property can not be set.\n"), fname);
        return 0;
    }

    if (!sgi_checkTypeOptionalInputArguments(fname, pvApiCtx)) return 0;
    if (!sgi_checkSizeOptionalInputArguments(fname, pvApiCtx)) return 0;

    variantArgs = sgi_getVariantsOptionalInputArguments(fname, pvApiCtx, &iErr, &nbArguments);
    if (iErr == 1) return 0;

    if (variantArgs)
    {
        InvokeError = ole_invoke(DISPATCH_PROPERTYPUT, NULL, pVariantDisp->pdispVal, propertyName, nbArguments, variantArgs);

        delete [] variantArgs;
        variantArgs = NULL;

        if (InvokeError != INVOKE_NO_ERROR)
        {
            char *msg_error = getInvokeErrorMessage(InvokeError);

            Scierror(999, "%s: error %s.\n", fname, msg_error);

            if (msg_error)
            {
                delete msg_error;
                msg_error = NULL;
            }
        }
        else
        {
            LhsVar(1) = 0;	
            PutLhsVar();
        }
    }
    else
    {
        Scierror(999,_("%s: No more memory.\n"), fname);
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
