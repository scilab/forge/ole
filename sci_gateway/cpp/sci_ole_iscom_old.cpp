/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
#include "stack-c.h"
#include "gw_automation.h"
#include "Scierror.h"
#include "localization.h"
#include "VariantHelper.hxx"
/*--------------------------------------------------------------------------*/
int sci_ole_iscom(char *fname, unsigned long /*fname_len*/)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	BOOL bIsVariant = FALSE;

	CheckRhs(1, 1);
	CheckLhs(0, 1);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (isPointerType(pvApiCtx, piAddrOne))
	{
        void* pvPtr = NULL;
		sciErr = getPointer(pvApiCtx, piAddrOne, &pvPtr);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if (isVariant(pvPtr))
		{
			bIsVariant = TRUE;
		}
	}

	if (createScalarBoolean(pvApiCtx, Rhs + 1, bIsVariant) == 0)
    {
        LhsVar(1) = Rhs + 1;	
        PutLhsVar();
    }
    else
    {
        Scierror(999,_("%s: No more memory.\n"),fname);
    }
	return 0;
}
/*--------------------------------------------------------------------------*/
