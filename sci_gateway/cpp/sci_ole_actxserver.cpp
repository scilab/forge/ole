/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "VariantsManager.hxx"
#include "Scierror.h"
#include "localization.h"
/*--------------------------------------------------------------------------*/
int sci_ole_actxserver(char *fname, void* pvApiCtx)
{
	IDispatch * pdispApplication = NULL;
	VARIANT *pVariantApplication = NULL;
	CLSID clsApplication;
	wchar_t *pStrApplicationName = NULL;
	SciErr sciErr;
	int* piAddr = NULL;
	wchar_t *pStrMachineName = NULL;

	CheckRhs(1, 3);
	CheckLhs(0, 1);

	if (Rhs == 2)
	{
		Scierror(999, "%s: Wrong number(s) input arguments.\n");
		return 0;
	}

	if (Rhs == 3)
	{
		int* piAddrTwo = NULL;
		int* piAddrThree = NULL;
		wchar_t*pStrArgTwo = NULL;
		wchar_t*pStrArgThree = NULL;
		
		sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddrTwo);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddrThree);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

        if (isScalar(pvApiCtx, piAddrTwo) == 0)
        {
            Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 2);
            return 0;
        }

        if (isScalar(pvApiCtx, piAddrThree) == 0)
        {
            Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 3);
            return 0;
        }

		if (getAllocatedSingleWideString(pvApiCtx, piAddrTwo, &pStrArgTwo) != 0)
        {
            Scierror(999,_("%s: No more memory.\n"),fname);
            return 0;
        }

        if (wcscmp(pStrArgTwo, L"machine") == 0)
        {
         
            if (getAllocatedSingleWideString(pvApiCtx, piAddrThree, &pStrArgThree) != 0)
            {
                freeAllocatedSingleWideString(pStrArgTwo);
                pStrArgTwo = NULL;

                Scierror(999,_("%s: No more memory.\n"),fname);
                return 0;
            }
            pStrMachineName = _wcsdup(pStrArgThree);
        }
        else
        {
            freeAllocatedSingleWideString(pStrArgTwo);
            pStrArgTwo = NULL;

            Scierror(999,_("%s: Wrong value for input argument #%d: 'machine' expected.\n"), fname, 2);
            return 0;
        }

        freeAllocatedSingleWideString(pStrArgTwo);
        pStrArgTwo = NULL;

        freeAllocatedSingleWideString(pStrArgThree);
        pStrArgTwo = NULL;
	}

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    if (isScalar(pvApiCtx, piAddr) == 0)
    {
        Scierror(999,_("%s: Wrong size for input argument #%d.\n"),fname, 1);
        return 0;
    }

	if (getAllocatedSingleWideString(pvApiCtx, piAddr, &pStrApplicationName) != 0)
    {
        Scierror(999,_("%s: No more memory.\n"), fname);
        return 0;
    }

	if (pStrApplicationName[0] == L'{')
	{
		if (FAILED(CLSIDFromString(pStrApplicationName, &clsApplication))) 
		{
			freeAllocatedSingleWideString(pStrApplicationName);
			pStrApplicationName = NULL;
			Scierror(999,_("%s: Error CLSIDFromString.\n"),fname);
			return 0;
		}
	}
	else
	{
		if (FAILED(CLSIDFromProgID(pStrApplicationName, &clsApplication))) 
		{
			freeAllocatedSingleWideString(pStrApplicationName);
			pStrApplicationName = NULL;
			Scierror(999,_("%s: Error CLSIDFromProgID.\n"),fname);
			return 0;
		}
	}


    freeAllocatedSingleWideString(pStrApplicationName);
    pStrApplicationName = NULL;

	if (pStrMachineName)
	{
		COSERVERINFO ServerInfo;
		ZeroMemory(&ServerInfo, sizeof(COSERVERINFO));

		COAUTHINFO athn;
		ZeroMemory(&athn, sizeof(COAUTHINFO));

		athn.dwAuthnLevel = RPC_C_AUTHN_LEVEL_NONE;
		athn.dwAuthnSvc = RPC_C_AUTHN_WINNT;
		athn.dwAuthzSvc = RPC_C_AUTHZ_NONE;
		athn.dwCapabilities = EOAC_NONE;
		athn.dwImpersonationLevel = RPC_C_IMP_LEVEL_IMPERSONATE;
		athn.pAuthIdentityData = NULL;
		athn.pwszServerPrincName = NULL;

		ServerInfo.pwszName = pStrMachineName; 
		ServerInfo.pAuthInfo = &athn;
		ServerInfo.dwReserved1 = 0;
		ServerInfo.dwReserved2 = 0;

		MULTI_QI qi = {0,0,0};
		ZeroMemory(&qi, sizeof(MULTI_QI));
		qi.pIID = &IID_IDispatch;
		

		if (FAILED(CoCreateInstanceEx(clsApplication, NULL, CLSCTX_REMOTE_SERVER, &ServerInfo, 1, &qi))) 
		{
            Scierror(999,_("%s: Error CoCreateInstanceEx.\n"),fname);
            return 0;
		}
		pdispApplication = (IDispatch*)qi.pItf;

        free(pStrMachineName);
        pStrMachineName = NULL;
	}
	else
	{
		if (FAILED(CoCreateInstance(clsApplication, NULL, CLSCTX_SERVER, IID_IDispatch, (void **)&pdispApplication))) 
		{
            Scierror(999,_("%s: Error CoCreateInstance.\n"),fname);
            return 0;
		}
	}
	
  	pVariantApplication = new VARIANT;
	VariantInit(pVariantApplication);
	pVariantApplication->vt = VT_DISPATCH;
	pVariantApplication->pdispVal = pdispApplication;

	addVariantInManager(pVariantApplication);
	sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)pVariantApplication);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	LhsVar(1) = Rhs + 1;	
	PutLhsVar();
	return 0;
}
/*--------------------------------------------------------------------------*/
