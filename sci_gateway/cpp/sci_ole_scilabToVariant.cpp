/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/*--------------------------------------------------------------------------*/
#define _WIN32_WINNT 0x0501
#define _WIN32_IE 0x0501
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "Scierror.h"
#include "localization.h"
#include "VariantHelper.hxx"
#include "VariantsManager.hxx"
/*--------------------------------------------------------------------------*/
static int sci_ole_mlistToVariant(char *fname, void* pvApiCtx);
static int sci_ole_stringsToVariant(char *fname, void* pvApiCtx);
static int sci_ole_booleanToVariant(char *fname, void* pvApiCtx);
static int sci_ole_matrixToVariant(char *fname, void* pvApiCtx);
static int sci_ole_intsToVariant(char *fname, void* pvApiCtx);
/*--------------------------------------------------------------------------*/
int sci_ole_scilabToVariant(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	int iType = 0;

	CheckRhs(1, 1);
	CheckLhs(0, 1);

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	sciErr = getVarType(pvApiCtx, piAddrOne, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	switch (iType)
	{
	case sci_matrix:
		{
			return sci_ole_matrixToVariant(fname, pvApiCtx);
		}
		break;	
	case sci_ints:
		{
			return sci_ole_intsToVariant(fname, pvApiCtx);
		}
		break;	
	case sci_boolean:
		{
			return sci_ole_booleanToVariant(fname, pvApiCtx);
		}
		break;
	case sci_strings:
		{
			return sci_ole_stringsToVariant(fname, pvApiCtx);
		}
		break;
	case sci_mlist:
		{
			return sci_ole_mlistToVariant(fname, pvApiCtx);
		}
		break;
	default:
		{
			Scierror(999, "type can not converted to VARIANT.\n");
		}
		break;
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
static int sci_ole_mlistToVariant(char* /*fname*/, void* pvApiCtx)
{
    int iItem = 0;
    int *piChild = NULL;
    int rows = 0;
    int cols = 0;
    int *lenghtStrings = NULL;
    char **pFieldNames = NULL;
    SciErr sciErr;
    int *piAddrOne = NULL;
    int iType = 0;

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getListItemNumber(pvApiCtx, piAddrOne, &iItem);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (iItem != 3)
    {
        Scierror(999, "type can not converted to VARIANT.\n");
        return 0;
    }

    sciErr = getListItemAddress(pvApiCtx, piAddrOne, 1, &piChild);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarType(pvApiCtx, piChild, &iType);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (iType != sci_strings)
    {
        Scierror(999, "type can not converted to VARIANT.\n");
        return 0;
    }

    sciErr = getMatrixOfStringInList(pvApiCtx, piAddrOne, 1, &rows, &cols, NULL, NULL);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if ((cols != 3) || (rows != 1))
    {
        Scierror(999, "type can not converted to VARIANT.\n");
        return 0;
    }

    lenghtStrings = new int[rows * cols];
    if (lenghtStrings == NULL)
    {
        Scierror(999, "memory allocation error.\n");
        return 0;
    }

    sciErr = getMatrixOfStringInList(pvApiCtx, piAddrOne, 1, &rows, &cols, lenghtStrings, NULL);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }


    pFieldNames = new char*[rows * cols];
    for(int i = 0 ; i < rows * cols ; i++)
    {
        pFieldNames[i] = new char[lenghtStrings[i] + 1];
    }

    sciErr = getMatrixOfStringInList(pvApiCtx, piAddrOne, 1, &rows, &cols, lenghtStrings, pFieldNames);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if ((strcmp(pFieldNames[0], "VARIANT") != 0) ||
        (strcmp(pFieldNames[1], "strings") != 0) ||
        (strcmp(pFieldNames[1], "doubles") != 0))
    {
        Scierror(999, "type can not converted to VARIANT.\n");
        for(int i = 0 ; i < rows * cols ; i++)
        {
            delete [] pFieldNames[i];
            pFieldNames[i] = NULL;
        }
        delete [] pFieldNames;
        pFieldNames = NULL;
//        return 0;
        int iItem = 0;
        int *piChild = NULL;
        int rows = 0;
        int cols = 0;
        int *lenghtStrings = NULL;
        char **pFieldNames = NULL;

        sciErr = getListItemNumber(pvApiCtx, piAddrOne, &iItem);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (iItem != 3)
        {
            Scierror(999, "type can not converted to VARIANT.\n");
            return 0;
        }

        sciErr = getListItemAddress(pvApiCtx, piAddrOne, 1, &piChild);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        sciErr = getVarType(pvApiCtx, piChild, &iType);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (iType != sci_strings)
        {
            Scierror(999, "type can not converted to VARIANT.\n");
            return 0;
        }

        sciErr = getMatrixOfStringInList(pvApiCtx, piAddrOne, 1, &rows, &cols, NULL, NULL);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if ((cols != 3) || (rows != 1))
        {
            Scierror(999, "type can not converted to VARIANT.\n");
            return 0;
        }

        lenghtStrings = new int[rows * cols];
        if (lenghtStrings == NULL)
        {
            Scierror(999, "memory allocation error.\n");
            return 0;
        }

        sciErr = getMatrixOfStringInList(pvApiCtx, piAddrOne, 1, &rows, &cols, lenghtStrings, NULL);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        pFieldNames = new char*[rows * cols];
        for(int i = 0 ; i < rows * cols ; i++)
        {
            pFieldNames[i] = new char[lenghtStrings[i] + 1];
        }

        sciErr = getMatrixOfStringInList(pvApiCtx, piAddrOne, 1, &rows, &cols, lenghtStrings, pFieldNames);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if ((strcmp(pFieldNames[0], "VARIANT") != 0) ||
            (strcmp(pFieldNames[1], "strings") != 0) ||
            (strcmp(pFieldNames[1], "doubles") != 0))
        {
            Scierror(999, "type can not converted to VARIANT.\n");
            for(int i = 0 ; i < rows * cols ; i++)
            {
                delete [] pFieldNames[i];
                pFieldNames[i] = NULL;
            }
            delete [] pFieldNames;
            pFieldNames = NULL;
            return 0;
        }
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
static int sci_ole_stringsToVariant(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
	wchar_t** pStrs = NULL;
	int rows = 0;
	int cols = 0;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (getAllocatedMatrixOfWideString(pvApiCtx,piAddrOne, &rows, &cols, &pStrs) == 0)
	{
		int iError = 0;
		VARIANT *pVariant =	NULL;

        if (isScalar(pvApiCtx,piAddrOne))
        {
            pVariant = wstringToVariant(pStrs[0], &iError);
        }
        else
        {
            pVariant = matrixOfWideStringToVariant((const wchar_t **)pStrs, rows, cols, &iError);
        }
        

		freeAllocatedMatrixOfWideString(rows, cols, pStrs);
		pStrs = NULL;

		if (iError == 0)
		{
			addVariantInManager(pVariant);
			sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)pVariant);
			if(sciErr.iErr)
			{
				printError(&sciErr, 0);
				return 0;
			}
			LhsVar(1) = Rhs + 1;	
			PutLhsVar();
		}
		else
		{
			Scierror(999, "%s: error conversion.\n", fname);
		}
	}
	else
	{
		Scierror(999, "%s: error memory allocation.\n", fname);
	}
	return 0;
}
/*--------------------------------------------------------------------------*/
static int sci_ole_booleanToVariant(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
    int m = 0;
    int n = 0;
    BOOL *pBoolValues = NULL;
    int iError = 0;
    VARIANT *pVariant =	NULL;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    sciErr = getMatrixOfBoolean(pvApiCtx, piAddrOne, &m, &n, &pBoolValues);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isScalar(pvApiCtx, piAddrOne))
    {
        pVariant = boolToVariant(pBoolValues[0], &iError);
    }
    else
    {
        pVariant = matrixOfBoolToVariant(pBoolValues, m, n, &iError);
    }

    if (iError == 0)
    {
        addVariantInManager(pVariant);
        sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)pVariant);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }
        LhsVar(1) = Rhs + 1;	
        PutLhsVar();
    }
    else
    {
        Scierror(999, "%s: error conversion.\n", fname);
    }

	return 0;
}
/*--------------------------------------------------------------------------*/
static int sci_ole_matrixToVariant(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
    int iError = 0;
    VARIANT *pVariant =	NULL;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    if (isEmptyMatrix(pvApiCtx, piAddrOne))
    {
        pVariant = emptyToVariant(&iError);
    }
    else
    {
         if (isVarComplex(pvApiCtx, piAddrOne))
         {
             Scierror(999,_("%s: Wrong type for input argument #%d: real expected.\n"),fname,1);
             return 0;
         }
         else
         {
             int m = 0;
             int n = 0;
             double *pDoubleValues = NULL;

             sciErr = getMatrixOfDouble(pvApiCtx, piAddrOne, &m, &n, &pDoubleValues);
             if(sciErr.iErr)
             {
                 printError(&sciErr, 0);
                 return 0;
             }

             if (isScalar(pvApiCtx, piAddrOne))
             {
                 pVariant = doubleToVariant(pDoubleValues[0], &iError);
             }
             else
             {
                 pVariant = matrixOfDoubleToVariant(pDoubleValues, m, n, &iError);
             }
         }
    }

    if (iError == 0)
    {
        addVariantInManager(pVariant);
        sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)pVariant);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }
        LhsVar(1) = Rhs + 1;	
        PutLhsVar();
    }
    else
    {
        Scierror(999, "%s: error conversion.\n", fname);
    }
	return 0;
}
/*--------------------------------------------------------------------------*/
static int sci_ole_intsToVariant(char *fname, void* pvApiCtx)
{
	SciErr sciErr;
	int* piAddrOne = NULL;
    
    int iError = 0;
    VARIANT *pVariant =	NULL;
    int m = 0;
    int n = 0;
    int iPrecision = 0;

	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

    sciErr = getMatrixOfIntegerPrecision(pvApiCtx, piAddrOne, &iPrecision);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    switch (iPrecision)
    {
        case SCI_INT8:
            {
                char *int8Values = NULL;
                sciErr = getMatrixOfInteger8(pvApiCtx, piAddrOne, &m, &n, &int8Values);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    return 0;
                }

                if (isScalar(pvApiCtx, piAddrOne))
                {
                    pVariant = int8ToVariant(int8Values[0], &iError);
                }
                else
                {
                    pVariant = matrixOfInt8ToVariant(int8Values, m, n, &iError);
                }
            }
            break;
        case SCI_INT16:
            {
                short *int16Values = NULL;
                sciErr = getMatrixOfInteger16(pvApiCtx, piAddrOne, &m, &n, &int16Values);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    return 0;
                }

                if (isScalar(pvApiCtx, piAddrOne))
                {
                    pVariant = int16ToVariant(int16Values[0], &iError);
                }
                else
                {
                    pVariant = matrixOfInt16ToVariant(int16Values, m, n, &iError);
                }
            }
            break;
        case SCI_INT32:
            {
                int *int32Values = NULL;
                sciErr = getMatrixOfInteger32(pvApiCtx, piAddrOne, &m, &n, &int32Values);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    return 0;
                }

                if (isScalar(pvApiCtx, piAddrOne))
                {
                    pVariant = int32ToVariant(int32Values[0], &iError);
                }
                else
                {
                    pVariant = matrixOfInt32ToVariant(int32Values, m, n, &iError);
                }
            }
            break;
        case SCI_UINT8:
            {
                unsigned char *unsignedint8Values = NULL;
                sciErr = getMatrixOfUnsignedInteger8(pvApiCtx, piAddrOne, &m, &n, &unsignedint8Values);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    return 0;
                }

                if (isScalar(pvApiCtx, piAddrOne))
                {
                    pVariant = unsignedint8ToVariant(unsignedint8Values[0], &iError);
                }
                else
                {
                    pVariant = matrixOfUnsignedInt8ToVariant(unsignedint8Values, m, n, &iError);
                }
            }
            break;
        case SCI_UINT16:
            {
                unsigned short *unsignedint16Values = NULL;
                sciErr = getMatrixOfUnsignedInteger16(pvApiCtx, piAddrOne, &m, &n, &unsignedint16Values);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    return 0;
                }

                if (isScalar(pvApiCtx, piAddrOne))
                {
                    pVariant = unsignedint16ToVariant(unsignedint16Values[0], &iError);
                }
                else
                {
                    pVariant = matrixOfUnsignedInt16ToVariant(unsignedint16Values, m, n, &iError);
                }
            }
            break;
        case SCI_UINT32:
            {
                unsigned int *unsignedint32Values = NULL;
                sciErr = getMatrixOfUnsignedInteger32(pvApiCtx, piAddrOne, &m, &n, &unsignedint32Values);
                if(sciErr.iErr)
                {
                    printError(&sciErr, 0);
                    return 0;
                }

                if (isScalar(pvApiCtx, piAddrOne))
                {
                    pVariant = unsignedint32ToVariant(unsignedint32Values[0], &iError);
                }
                else
                {
                    pVariant = matrixOfUnsignedInt32ToVariant(unsignedint32Values, m, n, &iError);
                }
            }
            break;
    }
   
    
    if (iError == 0)
    {
        addVariantInManager(pVariant);
        sciErr = createPointer(pvApiCtx, Rhs + 1, (void*)pVariant);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }
        LhsVar(1) = Rhs + 1;	
        PutLhsVar();
    }
    else
    {
        Scierror(999, "%s: error conversion.\n", fname);
    }

	return 0;
}
/*--------------------------------------------------------------------------*/
