/*--------------------------------------------------------------------------*/
/* Allan CORNET */
/* DIGITEO 2011 */
/*--------------------------------------------------------------------------*/
#include <Ole2.h>
#include "api_scilab.h"
//#include "stack-c.h"
#include "gw_automation.h"
#include "Scierror.h"
#include "localization.h"
#include "modeManager.hxx"
/*--------------------------------------------------------------------------*/
#define MODE_AUTO_ARG "auto"
#define MODE_VARIANT_ARG "variant"
/*--------------------------------------------------------------------------*/
int sci_ole_mode(char *fname, void* pvApiCtx)
{
    Rhs = Max(Rhs, 0);
    CheckRhs(0, 1);
    CheckLhs(0, 1);

    if (Rhs != 0) // Rhs == 1
    {
        int* piAddrOne = NULL;
        SciErr sciErr =  getVarAddressFromPosition(pvApiCtx, 1, &piAddrOne);

        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isScalar(pvApiCtx, piAddrOne))
        {
            if (isStringType(pvApiCtx, piAddrOne))
            {
                char *argMode = NULL;
                if (getAllocatedSingleString(pvApiCtx, piAddrOne, &argMode) != 0)
                {
                    Scierror(999,_("%s: Wrong type for input argument #%d: A String expected.\n"),fname, 1);
                    return 0;
                }

                if ((_stricmp(argMode, MODE_AUTO_ARG) == 0) || 
                   (_stricmp(argMode, MODE_VARIANT_ARG) == 0))
                {
                    if (_stricmp(argMode, MODE_AUTO_ARG) == 0)
                    {
                        setVariantModeConversion(true);
                    }
                    else
                    {
                        setVariantModeConversion(false);
                    }
                    freeAllocatedSingleString(argMode);
                    argMode = NULL;
                }
                else
                {
                    freeAllocatedSingleString(argMode);
                    argMode = NULL;
                    Scierror(999,_("%s: Wrong value for input argument #%d: '%s' or '%s' expected.\n"),fname, 1, "auto", "variant");
                    return 0;
                }
            }
            else
            {
                Scierror(999,_("%s: Wrong type for input argument #%d: A String expected.\n"),fname, 1);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A String expected.\n"),fname, 1);
            return 0;
        }
    }

    bool bMode = getVariantModeConversion();
    int iErr = 0;
    if (bMode)
    {
        iErr = createSingleString(pvApiCtx, Rhs + 1, "auto");
    }
    else
    {
        iErr = createSingleString(pvApiCtx, Rhs + 1, "variant");
    }

    if (iErr == 0)
    {
        LhsVar(1) = Rhs + 1;
        PutLhsVar();
    }

    return 0;
}
/*--------------------------------------------------------------------------*/
