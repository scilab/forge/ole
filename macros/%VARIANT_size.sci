// ============================================================================
// Allan CORNET - DIGITEO - 2011
// ============================================================================
function [m, n] = %VARIANT_size(VARIANT, opt)
  if argn(2) > 1 then
    m = size(VARIANT.value, opt);
  else
    m = size(VARIANT.value);
    if argn(1) > 1 then
      [m, n] = (m(1), m(2));
    end
  end
endfunction
// ============================================================================
