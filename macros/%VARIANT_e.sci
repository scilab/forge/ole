// ============================================================================
// Allan CORNET - DIGITEO - 2011
// ============================================================================
function R = %VARIANT_e(varargin)
  //extract mlist VARIANT
  s = varargin($);
  R = s.value;
  T = s.string;
  R = R(varargin(1:$-1));
  T = T(varargin(1:$-1));
  if and(isnan(R)) then
    R = T;
  end  
endfunction
// ============================================================================
